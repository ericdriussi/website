import { describe, it, expect } from "vitest"
import { ActivePath } from "../src/logic/ActivePath"
import * as fc from "fast-check"

describe("ActivePath should", () => {
	describe("validate", () => {
		it("identical paths", () => {
			fc.assert(
				fc.property(
					fc.array(aValidString(), { minLength: 1 }).map((arr) => arr.join("/")),
					(path) => {
						const allPossiblePaths = allSlashCombinations(path)

						allPossiblePaths.forEach((href) => {
							allPossiblePaths.forEach((pathname) => {
								expect(ActivePath.check(href, pathname)).toBeTruthy()
							})
						})
					},
				),
			)
		})

		it("pagination", () => {
			fc.assert(
				fc.property(
					fc.array(aValidString(), { minLength: 1 }).map((arr) => arr.join("/")),
					fc.integer(),
					(path, page) => {
						const allPossiblePaths = allSlashCombinations(path)

						allPossiblePaths.forEach((href) => {
							allPossiblePaths.forEach((pathname) => {
								expect(ActivePath.check(href, `${pathname}/${page}`)).toBeTruthy()
							})
						})
					},
				),
			)
		})

		it("subsections", () => {
			fc.assert(
				fc.property(
					fc.array(aValidString(), { minLength: 2 }).map((arr) => arr.join("/")),
					fc.array(aValidString()),
					(rootAndSection, subSection) => {
						const allPossibleSections = allSlashCombinations(rootAndSection)
						const allPossibleSubSections = generatePathnames(rootAndSection, subSection)

						allPossibleSections.forEach((href) => {
							allPossibleSubSections.forEach((pathname) => {
								expect(ActivePath.check(href, pathname)).toBeTruthy()
							})
						})
					},
				),
			)
		})
	})

	describe("invalidate", () => {
		it("different sections", () => {
			fc.assert(
				fc.property(
					fc
						.array(aValidString(), {
							maxLength: 1,
						})
						.map((arr) => arr.join("/")),
					fc.array(aValidString()),
					(root, section) => {
						const allPossibleSections = allSlashCombinations(root)
						const allPossibleSubSections = generatePathnames(root, section)

						allPossibleSections.forEach((href) => {
							allPossibleSubSections.forEach((pathname) => {
								expect(ActivePath.check(href, pathname)).toBeFalsy()
							})
						})
					},
				),
			)
		})

		it("pagination for different sections", () => {
			fc.assert(
				fc.property(
					fc.array(aValidString(), { minLength: 1 }).map((arr) => arr.join("/")),
					fc.array(aValidString()),
					fc.integer(),
					(root, section, page) => {
						const allPossibleSections = allSlashCombinations(root)
						const allPossibleSubSections = generatePathnames(root, section)

						allPossibleSections.forEach((href) => {
							allPossibleSubSections.forEach((pathname) => {
								expect(ActivePath.check(href, `${pathname}/${page}`)).toBeFalsy()
							})
						})
					},
				),
			)
		})
	})
})

function aValidString() {
	return fc.string().filter((str) => str.trim().length > 0 && !str.includes("/") && isNaN(Number(str)))
}

function allSlashCombinations(basePath: string) {
	return [`/${basePath}/`, `/${basePath}`, `${basePath}/`, `${basePath}`]
}

function generatePathnames(href: string, subPaths: string[]) {
	return subPaths.flatMap((subPath) => allSlashCombinations(`${href}/${subPath}`))
}
