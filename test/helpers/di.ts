import { PostService } from "../../src/logic/service/PostService"
import type { PostRepo } from "../../src/logic/storage/PostRepo"
import type { TaxonomyRepo } from "../../src/logic/storage/TaxonomyRepo"

export function initService(postRepo: PostRepo, taxonomyRepo: TaxonomyRepo): PostService {
	return new PostService(postRepo, taxonomyRepo)
}
