import { vi } from "vitest"
import type { Post } from "../../src/content/config"

export class MockPostRepo {
	value: Post[] = []

	private constructor(posts: Post[]) {
		this.value = posts
	}

	static returning(posts: Post[]) {
		return new MockPostRepo(posts)
	}

	static empty() {
		return new MockPostRepo([])
	}

	all = vi.fn(() => this.value)
	allBefore = vi.fn(() => this.value)
	allAfter = vi.fn(() => this.value)
	random = vi.fn(() => this.value[Math.floor(Math.random() * this.value.length)])
}
