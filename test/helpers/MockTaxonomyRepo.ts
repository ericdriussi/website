import { vi } from "vitest"
import type { Post } from "../../src/content/config"

export class MockTaxonomyRepo {
	value: Post[] = []

	static empty() {
		return new MockTaxonomyRepo()
	}

	allInSeries = vi.fn(() => this.value)
	allWithCategory = vi.fn(() => this.value)
	allWithSomeOf = vi.fn(() => this.value)
}
