import { vi } from "vitest"
import type { Post } from "../../src/content/config"
import type { CollectionWrapper } from "../../src/logic/storage/CollectionWrapper"

export class MockCollection implements CollectionWrapper {
	private value: Post[] = []

	private constructor(posts: Post[]) {
		this.value = posts
	}

	static Returning(posts: Post[]) {
		return new MockCollection(posts)
	}

	get = vi.fn(async (): Promise<Post[]> => {
		return this.value
	})
}
