import "./date.d.ts" // add utility functions to Date prototype
import type { Post } from "../../src/content/config"
import { faker } from "@faker-js/faker"
import fc from "fast-check"

// This exists to simplify the setup
// It's functionally equal to the Post type,
// just more flexible for testing
type MockPost = {
	data: {
		published: Date
		category?: string
		tags?: string[]
		series?: string
		title?: string
	}
}

export class PostBuilder {
	private post: MockPost = { data: { published: new Date() } }

	withPublished(published: Date): PostBuilder {
		this.post.data.published = published
		return this
	}

	withCategory(category: string): PostBuilder {
		this.post.data.category = category
		return this
	}

	withTags(tags: string[]): PostBuilder {
		this.post.data.tags = tags
		return this
	}

	withSeries(series: string): PostBuilder {
		this.post.data.series = series
		return this
	}

	withTitle(title: string): PostBuilder {
		this.post.data.title = title
		return this
	}

	withRandomTitle(): PostBuilder {
		this.post.data.title = `${faker.word.noun()}-${faker.string.uuid()}`
		return this
	}

	withRandomPubDate(): PostBuilder {
		const start = new Date(1970, 1, 1)
		const end = new Date().aDayLess()
		this.post.data.published = faker.date.between({ from: start, to: end })
		return this
	}

	build() {
		return {
			data: {
				published: this.post.data.published,
				category: this.post.data.category,
				tags: this.post.data.tags,
				series: this.post.data.series,
				title: this.post.data.title,
			},
		} as Post
	}

	static randomPost(): Post {
		return new PostBuilder()
			.withRandomPubDate()
			.withRandomTitle()
			.withSeries(faker.lorem.word())
			.withCategory(faker.lorem.word())
			.withTags([faker.lorem.word(), faker.lorem.word()])
			.build()
	}

	static asArbitrary(): fc.Arbitrary<Post> {
		return fc.record({
			data: fc.record({
				title: fc.string({ minLength: 1 }),
				series: fc.string({ minLength: 1 }),
				published: fc.date({ min: new Date(1970, 1, 1), max: new Date() }),
				tags: fc.uniqueArray(fc.string({ minLength: 1 }), {
					minLength: 1,
					comparator: "IsStrictlyEqual",
				}),
				category: fc.string({ minLength: 1 }),
			}),
		}) as fc.Arbitrary<Post>
	}
}
