interface Date {
	aDayMore(): Date
	aDayLess(): Date
	daysLess(days: number): Date
}

Date.prototype.aDayMore = function () {
	const newDate = new Date(this.getTime())
	newDate.setDate(newDate.getDate() + 1)
	return newDate
}

Date.prototype.aDayLess = function () {
	const newDate = new Date(this.getTime())
	newDate.setDate(newDate.getDate() - 1)
	return newDate
}

Date.prototype.daysLess = function (days: number) {
	const newDate = new Date(this.getTime())
	newDate.setDate(newDate.getDate() - days)
	return newDate
}
