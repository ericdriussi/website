export class Scroll {
	static scrollDown(amount: number) {
		window.scrollY += amount
		window.dispatchEvent(new Event("scroll"))
	}

	static scrollUp(amount: number) {
		window.scrollY -= amount
		window.dispatchEvent(new Event("scroll"))
	}
}
