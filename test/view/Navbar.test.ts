import { describe, it, expect, beforeEach, afterEach, vi } from "vitest"
import { loadNavbarPeekabooEvent } from "../../src/logic/view/Navbar"
import { uiConfig } from "../../src/consts"
import { Scroll } from "./ScrollHelper"

describe("Navbar should", () => {
	let header: HTMLElement

	beforeEach(() => {
		window.scrollY = 50
		header = document.createElement("header")
		document.body.appendChild(header)
		vi.stubGlobal("requestAnimationFrame", (cb: FrameRequestCallback) => cb(0))
	})

	afterEach(() => {
		document.body.innerHTML = ""
		vi.unstubAllGlobals()
	})

	it("dissappear when scrolling down", () => {
		loadNavbarPeekabooEvent()

		Scroll.scrollDown(10)

		expect(header.style.top).toBe(`-${uiConfig.navbarHeight}`)
	})

	it("appear when scrolling up", () => {
		loadNavbarPeekabooEvent()

		Scroll.scrollUp(10)

		expect(header.style.top).toBe("0px")
	})

	it("do nothing if no header is found", () => {
		document.body.removeChild(header)
		loadNavbarPeekabooEvent()

		Scroll.scrollDown(10)

		expect(header.style.top).toBe("")
	})
})
