import { describe, it, expect, beforeEach, vi, afterEach } from "vitest"
import { loadStickyHeadersOnScrollEvent } from "../../src/logic/view/StickyHeadings"
import { uiConfig } from "../../src/consts"
import { Scroll } from "./ScrollHelper"

describe("StickyHeadings should", () => {
	const headingHeight = 50
	let heading: HTMLHeadingElement

	beforeEach(() => {
		heading = document.createElement("h2")
		Object.defineProperty(heading, "offsetTop", { value: headingHeight * 2, configurable: true })
		Object.defineProperty(heading, "offsetHeight", { value: headingHeight, configurable: true })
		Object.defineProperty(heading, "getBoundingClientRect", {
			value: () => ({ top: headingHeight, bottom: headingHeight * 2, height: headingHeight }),
			configurable: true,
		})
		document.body.appendChild(heading)
		vi.stubGlobal("requestAnimationFrame", (cb: FrameRequestCallback) => cb(0))
	})

	afterEach(() => {
		document.body.innerHTML = ""
		vi.unstubAllGlobals()
	})

	it("stickify headers when scrolling down past their original position", () => {
		window.scrollY = headingHeight
		loadStickyHeadersOnScrollEvent()

		Scroll.scrollDown(10)

		expect(heading.style.position).toBe("sticky")
		expect(heading.style.top).toBe("-50px")
	})

	it("unstickify headers when scrolling up above their original position", () => {
		window.scrollY = headingHeight
		loadStickyHeadersOnScrollEvent()

		Scroll.scrollDown(10)
		Scroll.scrollUp(20)

		expect(heading.style.position).toBe("static")
		expect(heading.style.top).toBe(uiConfig.navbarHeight)
	})
})
