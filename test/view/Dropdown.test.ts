import { describe, it, expect, beforeEach, vi } from "vitest"
import { loadDropdownCloseOnClickEvent } from "../../src/logic/view/Dropdown"

describe("Dropdown should", () => {
	let toggle: HTMLInputElement

	beforeEach(() => {
		document.body.innerHTML = `
			<form class="form">
				<input type="checkbox" id="toggle" />
			</form>
		`

		toggle = document.getElementById("toggle") as HTMLInputElement
		loadDropdownCloseOnClickEvent()
	})

	it("close the open dropdown when clicking outside", () => {
		// start with open dropdown
		toggle.checked = true

		clickOutside()

		expect(toggle.checked).toBe(false)
	})

	it("do nothing with the open dropdown when clicking inside", () => {
		// start with open dropdown
		toggle.checked = true

		clickInside()

		expect(toggle.checked).toBe(true)
	})

	it("do nothing with the closed dropdown when clicking outside", () => {
		// start with closed dropdown
		toggle.checked = false

		clickOutside()

		expect(toggle.checked).toBe(false)
	})

	function clickOutside() {
		vi.spyOn(Node.prototype, "contains").mockReturnValue(false)
		document.dispatchEvent(new MouseEvent("mousedown"))
	}

	function clickInside() {
		vi.spyOn(Node.prototype, "contains").mockReturnValue(true)
		document.dispatchEvent(new MouseEvent("mousedown"))
	}
})
