import { ScrollHandler } from "../../src/logic/view/ScrollHandler"
import { describe, it, expect, vi, beforeEach, afterEach, type Mock } from "vitest"
import { Scroll } from "./ScrollHelper"

describe("ScrollHandler should", () => {
	let upCallback: Mock
	let downCallback: Mock

	beforeEach(() => {
		window.scrollY = 50
		upCallback = vi.fn()
		downCallback = vi.fn()
		vi.stubGlobal("requestAnimationFrame", (cb: FrameRequestCallback) => cb(0))
	})

	afterEach(() => {
		vi.restoreAllMocks()
		vi.unstubAllGlobals()
	})

	describe("call", () => {
		it("callback on scroll up", () => {
			ScrollHandler.init().onScrollUp(upCallback).registerEvents()

			Scroll.scrollUp(10)

			expect(upCallback).toHaveBeenCalledOnce()
		})

		it("callback on scroll down", () => {
			ScrollHandler.init().onScrollDown(downCallback).registerEvents()

			Scroll.scrollDown(10)

			expect(downCallback).toHaveBeenCalledOnce()
		})

		it("multiple callbacks scrolling up and down", () => {
			const secondUpCallback = vi.fn()
			const secondDownCallback = vi.fn()

			ScrollHandler.init()
				.onScrollUp(upCallback)
				.onScrollUp(secondUpCallback)
				.onScrollDown(downCallback)
				.onScrollDown(secondDownCallback)
				.registerEvents()

			Scroll.scrollUp(10)
			Scroll.scrollDown(10)

			expect(downCallback).toHaveBeenCalledOnce()
			expect(secondDownCallback).toHaveBeenCalledOnce()
			expect(upCallback).toHaveBeenCalledOnce()
			expect(secondUpCallback).toHaveBeenCalledOnce()
		})
	})

	describe("not call", () => {
		it("callbacks for small scroll changes", () => {
			ScrollHandler.init().onScrollUp(upCallback).onScrollDown(downCallback).registerEvents()

			Scroll.scrollUp(0.5)
			Scroll.scrollDown(0.5)

			expect(upCallback).not.toHaveBeenCalled()
			expect(downCallback).not.toHaveBeenCalled()
		})
	})
})
