import { SearchButton } from "../../src/logic/view/SearchButton.ts"
import { describe, it, expect, beforeEach } from "vitest"

describe("SearchBar should", () => {
	let searchButton: HTMLElement
	let searchBar: HTMLElement
	let searchBarStyle: CSSStyleDeclaration
	const searchBarId = "search-bar"

	beforeEach(() => {
		document.body.innerHTML = `
			<li id="search-button">
				<search-button data-searchbarid=${searchBarId}>
					<!-- SearchButton -->
				</search-button>
			</li>
			<search-bar id=${searchBarId} style="display: none;">
				<!-- SearchBar -->
			</search-bar>
		`

		searchButton = document.getElementById("search-button") as HTMLElement
		document.body.appendChild(searchButton)
		searchBar = document.getElementById(searchBarId) as HTMLElement
		document.body.appendChild(searchBar)
		searchBarStyle = window.getComputedStyle(searchBar)
	})

	it("not be displayed by default", () => {
		registerSUT()

		const searchBarStyle = window.getComputedStyle(searchBar)
		expect(searchBarStyle.display).toBe("none")
	})

	it("be displayed on click", () => {
		registerSUT()

		searchButton?.click()

		const searchBarStyle = window.getComputedStyle(searchBar)
		expect(searchBarStyle.display).toBe("block")
	})

	it("be hidden on second click", () => {
		registerSUT()

		searchButton?.click()
		searchButton?.click()

		expect(searchBarStyle.display).toBe("none")
	})

	it("be hidden on escape key", () => {
		registerSUT()

		searchButton?.click()
		document.dispatchEvent(new KeyboardEvent("keydown", { key: "Escape" }))

		expect(searchBarStyle.display).toBe("none")
	})

	function registerSUT() {
		if (!customElements.get("search-button")) {
			customElements.define("search-button", SearchButton)
		}
	}
})
