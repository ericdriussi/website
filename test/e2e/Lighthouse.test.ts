import { playAudit } from "playwright-lighthouse"
import playwright from "playwright"
import { test } from "@playwright/test"
import { dev } from "../../src/consts"

const ACCEPTABLE_SCORE = 85
test("Lighthouse audit should give acceptable scores", async () => {
	const browser = await playwright["chromium"].launch({
		args: [`--remote-debugging-port=${dev.e2eDebugPort}`],
	})
	const page = await browser.newPage()
	await page.goto(`http://localhost:${dev.serverPort}/`)

	await playAudit({
		page: page,
		thresholds: {
			performance: ACCEPTABLE_SCORE,
			accessibility: ACCEPTABLE_SCORE,
			"best-practices": ACCEPTABLE_SCORE,
			seo: ACCEPTABLE_SCORE,
		},
		port: dev.e2eDebugPort,
	})

	await browser.close()
})
