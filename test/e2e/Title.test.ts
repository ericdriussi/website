import { test, expect } from "@playwright/test"
import { site } from "src/consts"

test("Page should have a title", async ({ page }) => {
	await page.goto("/")
	await expect(page).toHaveTitle(site.title)
})
