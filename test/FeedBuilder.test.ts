import { describe, it, expect, vi } from "vitest"
import { feedOf } from "../src/logic/FeedBuilder"
import { site } from "../src/consts"
import { PostBuilder } from "./helpers/PostBuilder"

describe("FeedBuilder should", async () => {
	it("build a structured rss feed", async () => {
		const aPost = PostBuilder.randomPost()
		const aPostContent = "Post 1"
		aPost.body = `# ${aPostContent}\n`
		aPost.render = vi.fn()

		const result = feedOf([aPost])

		expect(result).toStrictEqual({
			title: site.title,
			description: site.description,
			site: site.url,
			items: [
				{
					title: aPost.data.title,
					link: `/${aPost.collection}/${aPost.slug}/`,
					pubDate: aPost.data.published,
					description: aPost.data.description,
					content: `<h1>${aPostContent}</h1>\n`,
				},
			],
		})
	})
})
