import "../helpers/date.d.ts" // add utility functions to Date prototype
import { describe, it, expect } from "vitest"
import { MockCollection } from "../helpers/MockCollection.ts"
import { PostBuilder } from "../helpers/PostBuilder.ts"
import type { Post } from "../../src/content/config.ts"
import { PostRepo } from "../../src/logic/storage/PostRepo.ts"
import { faker } from "@faker-js/faker"

describe("Post Repo should", async () => {
	const today = new Date()

	it("get all posts including drafts (only works in dev env)", async () => {
		const todaysPost = new PostBuilder().withPublished(today).build()
		const yesterdaysPost = new PostBuilder().withPublished(today.aDayLess()).build()
		const oldestPost = new PostBuilder().withPublished(today.aDayLess().aDayLess()).build()
		const todaysDraft = { data: { published: today } }
		const yesterdaysDraft = { data: { published: today.aDayLess() } }
		const posts = shuffled([oldestPost, todaysPost, yesterdaysPost, todaysDraft, yesterdaysDraft] as Post[])
		const postRepo = PostRepo.for(MockCollection.Returning(posts))

		const allPosts = await postRepo.all()

		expect(new Set(allPosts)).toStrictEqual(new Set(posts))
	})

	describe("fetch by date", async () => {
		const aDate = faker.date.anytime()

		it("all before a given date", async () => {
			const oldestPost = new PostBuilder().withPublished(aDate.aDayLess().aDayLess()).build()
			const postBefore = new PostBuilder().withPublished(aDate.aDayLess()).build()
			const postAfter = new PostBuilder().withPublished(aDate.aDayMore()).build()
			const postRepo = PostRepo.for(MockCollection.Returning([postBefore, postAfter, oldestPost]))

			const postsBefore = await postRepo.allBefore(aDate)

			expect(new Set(postsBefore)).toStrictEqual(new Set([oldestPost, postBefore]))
		})

		it("all after a given date", async () => {
			const postBefore = new PostBuilder().withPublished(aDate.aDayLess()).build()
			const postAfter = new PostBuilder().withPublished(aDate.aDayMore()).build()
			const newestPost = new PostBuilder().withPublished(aDate.aDayMore().aDayMore()).build()
			const postRepo = PostRepo.for(MockCollection.Returning([postBefore, postAfter, newestPost]))

			const postsAfter = await postRepo.allAfter(aDate)

			expect(new Set(postsAfter)).toStrictEqual(new Set([postAfter, newestPost]))
		})
	})

	it("fetch random posts", async () => {
		const posts = [new PostBuilder().build(), new PostBuilder().build()]
		const postRepo = PostRepo.for(MockCollection.Returning(posts))

		const randomPost = await postRepo.random()

		expect(posts).toContain(randomPost)
	})
})

function shuffled(array: Post[]): Post[] {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1))
		;[array[i], array[j]] = [array[j], array[i]]
	}
	return array
}
