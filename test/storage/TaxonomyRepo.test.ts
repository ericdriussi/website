import { describe, it, expect } from "vitest"
import { MockCollection } from "../helpers/MockCollection"
import { PostBuilder } from "../helpers/PostBuilder"
import { TaxonomyRepo } from "../../src/logic/storage/TaxonomyRepo"

describe("TaxonomyRepo should", async () => {
	it("get series alphabetically", async () => {
		const seriesA = "Series A"
		const postWithSeriesA = new PostBuilder().withSeries(seriesA).build()
		const seriesB = "Series B"
		const postWithSeriesB = new PostBuilder().withSeries(seriesB).build()
		const posts = [postWithSeriesA, postWithSeriesB]
		const taxonomyRepo = TaxonomyRepo.for(MockCollection.Returning(posts))

		const sortedSeries = await taxonomyRepo.sortedSeries()

		expect(sortedSeries).toStrictEqual([seriesA, seriesB])
	})

	it("get categories with count", async () => {
		const categoryA = "Category A"
		const postWithCategoryA = new PostBuilder().withCategory(categoryA).build()
		const categoryB = "Category B"
		const postWithCategoryB = new PostBuilder().withCategory(categoryB.toLowerCase()).build()
		const anotherPostWithCategoryB = new PostBuilder().withCategory(categoryB.toUpperCase()).build()
		const posts = [postWithCategoryB, postWithCategoryA, anotherPostWithCategoryB]
		const taxonomyRepo = TaxonomyRepo.for(MockCollection.Returning(posts))

		const categoriesWithCount = await taxonomyRepo.categoriesWithCount()

		expect(categoriesWithCount).toStrictEqual([
			{ taxonomy: categoryB.toLowerCase(), count: 2 },
			{ taxonomy: categoryA.toLowerCase(), count: 1 },
		])
	})

	it("get tags with count", async () => {
		const tagA = "Tag A"
		const postWithTagA = new PostBuilder().withTags([tagA.toLowerCase()]).build()
		const tagB = "Tag B"
		const postWithTagB = new PostBuilder().withTags([tagA.toUpperCase(), tagB]).build()
		const anotherPostWithTagB = new PostBuilder().withTags([tagB]).build()
		const posts = [postWithTagB, postWithTagA, anotherPostWithTagB]
		const taxonomyRepo = TaxonomyRepo.for(MockCollection.Returning(posts))

		const tagsWithCount = await taxonomyRepo.tagsWithCount()

		expect(tagsWithCount).toStrictEqual([
			{ taxonomy: tagA.toLowerCase(), count: 2 },
			{ taxonomy: tagB.toLowerCase(), count: 2 },
		])
	})

	it.each([
		(taxonomyRepo: TaxonomyRepo) => taxonomyRepo.sortedSeries(),
		(taxonomyRepo: TaxonomyRepo) => taxonomyRepo.categoriesWithCount(),
		(taxonomyRepo: TaxonomyRepo) => taxonomyRepo.tagsWithCount(),
	])("discarding invalid taxonomies", async (call) => {
		const postWithoutTaxonomies = new PostBuilder().build()
		const postWithEmptyTaxonomies = new PostBuilder().withCategory("").withSeries("").withTags([]).build()
		const posts = [postWithoutTaxonomies, postWithEmptyTaxonomies]
		const taxonomyRepo = TaxonomyRepo.for(MockCollection.Returning(posts))

		const result = await call(taxonomyRepo)

		expect(result).toStrictEqual([])
	})

	describe("get all posts", async () => {
		it("in series", async () => {
			const series = "Series"
			const postWithSeries = new PostBuilder().withSeries(series).build()
			const postWithoutSeries = new PostBuilder().build()
			const postWithOtherSeries = new PostBuilder().withSeries("Other Series").build()
			const posts = [postWithSeries, postWithoutSeries, postWithOtherSeries]
			const taxonomyRepo = TaxonomyRepo.for(MockCollection.Returning(posts))

			const allInSeries = await taxonomyRepo.allInSeries(series)

			expect(allInSeries).toStrictEqual([postWithSeries])
		})

		it("with category", async () => {
			const category = "Category"
			const postWithCategory = new PostBuilder().withCategory(category).build()
			const postWithoutCategory = new PostBuilder().build()
			const postWithOtherCategory = new PostBuilder().withCategory("Other Category").build()
			const posts = [postWithCategory, postWithoutCategory, postWithOtherCategory]
			const taxonomyRepo = TaxonomyRepo.for(MockCollection.Returning(posts))

			const allWithCategory = await taxonomyRepo.allWithCategory(category)

			expect(allWithCategory).toStrictEqual([postWithCategory])
		})

		it("with some of tags", async () => {
			const tag = "Tag1"
			const tags = [tag, "Tag2"]
			const postWithTag = new PostBuilder().withTags([tag]).build()
			const postWithoutTag = new PostBuilder().build()
			const postWithOtherTag = new PostBuilder().withTags(["Tag3"]).build()
			const posts = [postWithTag, postWithoutTag, postWithOtherTag]
			const taxonomyRepo = TaxonomyRepo.for(MockCollection.Returning(posts))

			const allWithTag = await taxonomyRepo.allWithSomeOf(tags)

			expect(allWithTag).toStrictEqual([postWithTag])
		})
	})

	describe("get no posts when", async () => {
		it.each([
			["series", (taxonomyRepo: TaxonomyRepo) => taxonomyRepo.allInSeries("series")],
			["category", (taxonomyRepo: TaxonomyRepo) => taxonomyRepo.allWithCategory("category")],
			["tag", (taxonomyRepo: TaxonomyRepo) => taxonomyRepo.allWithSomeOf(["tag"])],
		])("no matching %s is found", async (_, call) => {
			const postWithoutTaxonomies = new PostBuilder().build()
			const postWithEmptyTaxonomies = new PostBuilder().withCategory("").withSeries("").withTags([]).build()
			const posts = [postWithoutTaxonomies, postWithEmptyTaxonomies]
			const taxonomyRepo = TaxonomyRepo.for(MockCollection.Returning(posts))

			const result = await call(taxonomyRepo)

			expect(result).toStrictEqual([])
		})

		it.each([
			["series", (taxonomyRepo: TaxonomyRepo) => taxonomyRepo.allInSeries("")],
			["category", (taxonomyRepo: TaxonomyRepo) => taxonomyRepo.allWithCategory("")],
			["tags", (taxonomyRepo: TaxonomyRepo) => taxonomyRepo.allWithSomeOf([])],
			["tag", (taxonomyRepo: TaxonomyRepo) => taxonomyRepo.allWithSomeOf([""])],
		])("matches empty %s", async (_, call) => {
			const postWithEmptyTaxonomies = new PostBuilder().withCategory("").withSeries("").withTags([]).build()
			const postWithAnEmptyTag = new PostBuilder().withTags([""]).build()
			const posts = [postWithEmptyTaxonomies, postWithAnEmptyTag]
			const taxonomyRepo = TaxonomyRepo.for(MockCollection.Returning(posts))

			const result = await call(taxonomyRepo)

			expect(result).toStrictEqual([])
		})

		it.each([
			["series is", (taxonomyRepo: TaxonomyRepo) => taxonomyRepo.allInSeries(undefined)],
			["category is", (taxonomyRepo: TaxonomyRepo) => taxonomyRepo.allWithCategory(undefined)],
			["tags are", (taxonomyRepo: TaxonomyRepo) => taxonomyRepo.allWithSomeOf(undefined)],
		])("undefined %s requested", async (_, call) => {
			const postWithEmptyTaxonomies = new PostBuilder().withCategory("").withSeries("").withTags([]).build()
			const postWithAnEmptyTag = new PostBuilder().withTags([""]).build()
			const posts = [postWithEmptyTaxonomies, postWithAnEmptyTag]
			const taxonomyRepo = TaxonomyRepo.for(MockCollection.Returning(posts))

			const result = await call(taxonomyRepo)

			expect(result).toStrictEqual([])
		})
	})
})
