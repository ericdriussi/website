import { describe, it, expect } from "vitest"
import { Slugifier } from "../src/logic/Slugifier"

describe("Slugifier should", () => {
	it("trim", () => {
		const text = "  a text with leading spaces  "

		const result = Slugifier.slugify(text)

		expect(result).toEqual("a-text-with-leading-spaces")
	})

	it("lowercase", () => {
		const text = "A Text With Upper Case"

		const result = Slugifier.slugify(text)

		expect(result).toEqual("a-text-with-upper-case")
	})

	it("replace spaces with dashes", () => {
		const text = "a text with spaces"

		const result = Slugifier.slugify(text)

		expect(result).toEqual("a-text-with-spaces")
	})

	it("remove leading and trailing dashes", () => {
		const text = "-a text with dashes-"

		const result = Slugifier.slugify(text)

		expect(result).toEqual("a-text-with-dashes")
	})
})
