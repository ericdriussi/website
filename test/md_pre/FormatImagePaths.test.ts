import { remark } from "remark"
import { describe, it, expect } from "vitest"
import { imagePathFormatter } from "../../src/logic/md_pre/FormatImagePaths"

describe("ImagePathFormatter Remark Plugin should", () => {
	it("format image with relative path in cwd", async () => {
		const markdownInput = `![Alt text](./image.png)\n`
		const expectedOutput = `![Alt text](../../assets/content/image.png)\n`

		const result = await remark().use(imagePathFormatter()).process(markdownInput)

		expect(result.toString()).toBe(expectedOutput)
	})

	it.each([
		["not format image with relative path in parent dir", "../image.png"],
		["not format image with relative path elsewhere", "../../../image.png"],
		["not format image with absolute path", "/image.png"],
		["not format image with external path", "http://example.com/image.png"],
	])("%s", async (_, markdownPath) => {
		const markdownInput = `![Alt text](${markdownPath})\n`
		const expectedOutput = markdownInput

		const result = await remark().use(imagePathFormatter()).process(markdownInput)

		expect(result.toString()).toBe(expectedOutput)
	})
})
