import { remark } from "remark"
import { describe, it, expect } from "vitest"
import { h1HeadingsRemover } from "../../src/logic/md_pre/RemoveH1Headings"

describe("h1HeadingsRemover Remark Plugin should", () => {
	it("ignore files without h1 headings", async () => {
		const markdownInput = `## not a h1 heading\n`
		const expectedOutput = `## not a h1 heading\n`

		const result = await remark().use(h1HeadingsRemover()).process(markdownInput)

		expect(result.toString()).toBe(expectedOutput)
	})

	it("add a level to headings when h1 heading is present", async () => {
		const markdownInput = `# h1 heading\n`
		const expectedOutput = `## h1 heading\n`

		const result = await remark().use(h1HeadingsRemover()).process(markdownInput)

		expect(result.toString()).toBe(expectedOutput)
	})

	it("add a level to headings when h1 heading is present but not the first", async () => {
		const markdownInput = `## h2 heading\n\n# h1 heading\n`
		const expectedOutput = `### h2 heading\n\n## h1 heading\n`

		const result = await remark().use(h1HeadingsRemover()).process(markdownInput)

		expect(result.toString()).toBe(expectedOutput)
	})

	it("add a level to all headings when h1 heading", async () => {
		const markdownInput = `# h1 heading\n\n## h2 heading\n\n### h3 heading\n\n#### h4 heading\n\n##### h5 heading\n\n###### h6 heading\n`
		const expectedOutput = `## h1 heading\n\n### h2 heading\n\n#### h3 heading\n\n##### h4 heading\n\n###### h5 heading\n\n###### h6 heading\n`

		const result = await remark().use(h1HeadingsRemover()).process(markdownInput)

		expect(result.toString()).toBe(expectedOutput)
	})
})
