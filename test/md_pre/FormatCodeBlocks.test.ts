import { remark } from "remark"
import { describe, it, expect } from "vitest"
import { codeBlocksFormatter } from "../../src/logic/md_pre/FormatCodeBlocks"

describe("FormatCodeBlocks Remark Plugin should", () => {
	it("show line numbers in code blocks longer than 9 lines", async () => {
		const markdownInput = "```java\n" + "some_code\n".repeat(10) + "```\n"

		const result = await remark().use(codeBlocksFormatter()).process(markdownInput)

		expect(result.toString()).toContain("showLineNumbers=true")
	})

	it("not show line numbers in code blocks shorter than 10 lines", async () => {
		const markdownInput = "```c\n" + "some_code\n".repeat(9) + "```\n"

		const result = await remark().use(codeBlocksFormatter()).process(markdownInput)

		expect(result.toString()).toContain("showLineNumbers=false")
	})

	it("not show frame in code blocks without language", async () => {
		const markdownInput = "```\n" + "some_code\n" + "```\n"

		const result = await remark().use(codeBlocksFormatter()).process(markdownInput)

		expect(result.toString()).not.toContain('frame="terminal"')
		expect(result.toString()).not.toContain("title=")
	})

	it("show frame in code blocks with language", async () => {
		const markdownInput = "```python\n" + "some_code\n" + "```\n"

		const result = await remark().use(codeBlocksFormatter()).process(markdownInput)

		expect(result.toString()).toContain('frame=terminal title="python"')
	})
})
