import "../helpers/date.d.ts" // add utility functions to Date prototype
import { beforeEach, describe, expect, it, vi } from "vitest"
import { PostBuilder } from "../helpers/PostBuilder"
import { initService } from "../helpers/di"
import { MockPostRepo } from "../helpers/MockPostRepo.ts"
import { MockTaxonomyRepo } from "../helpers/MockTaxonomyRepo.ts"
import { PostSorter } from "../../src/logic/service/PostSorter.ts"
import { RelatedPostsMerger } from "../../src/logic/service/RelatedPostsMerger.ts"

describe("Post Service should fetch posts related to a given post", async () => {
	const bySimilaritySpy = vi.spyOn(PostSorter, "bySimilarity")
	const newestFirstSpy = vi.spyOn(PostSorter, "newestFirts")
	const oldestFirstSpy = vi.spyOn(PostSorter, "oldestFirst")
	const mergeSpy = vi.spyOn(RelatedPostsMerger, "merge")
	let postRepo: MockPostRepo
	let taxonomyRepo: MockTaxonomyRepo

	beforeEach(() => {
		postRepo = MockPostRepo.empty()
		taxonomyRepo = MockTaxonomyRepo.empty()
	})

	const amount = 5
	const inputPost = PostBuilder.randomPost()

	it("calling sorter and merger", async () => {
		postRepo.allAfter.mockResolvedValue([PostBuilder.randomPost()])
		postRepo.allBefore.mockResolvedValue([PostBuilder.randomPost()])
		taxonomyRepo.allInSeries.mockResolvedValue([PostBuilder.randomPost()])
		taxonomyRepo.allWithCategory.mockResolvedValue([PostBuilder.randomPost()])
		taxonomyRepo.allWithSomeOf.mockResolvedValue([PostBuilder.randomPost()])
		// @ts-ignore
		const service = initService(postRepo, taxonomyRepo)

		await service.relatedTo(inputPost, amount)

		expect(oldestFirstSpy).toHaveBeenCalledOnce()
		expect(newestFirstSpy).toHaveBeenCalledOnce()
		expect(bySimilaritySpy).toHaveBeenCalledTimes(3)
		expect(mergeSpy).toHaveBeenCalledOnce()
	})

	it("filling with random posts when not enough related", async () => {
		const moreThanEnoughPosts = Array(amount).fill(PostBuilder.randomPost())
		postRepo.allAfter.mockResolvedValue(moreThanEnoughPosts)
		postRepo.allBefore.mockResolvedValue(moreThanEnoughPosts)
		taxonomyRepo.allInSeries.mockResolvedValue([PostBuilder.randomPost()])
		taxonomyRepo.allWithCategory.mockResolvedValue([])
		taxonomyRepo.allWithSomeOf.mockResolvedValue([])
		postRepo.random.mockImplementationOnce(() => PostBuilder.randomPost())
		postRepo.random.mockImplementationOnce(() => PostBuilder.randomPost())
		// @ts-ignore
		const service = initService(postRepo, taxonomyRepo)

		const related = await service.relatedTo(inputPost, amount)

		expect(postRepo.random).toHaveBeenCalledTimes(2)
		expect(related.length).toBe(amount)
	})
})
