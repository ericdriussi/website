import { assert, asyncProperty, record, integer, constant, constantFrom, uniqueArray } from "fast-check"
import { it, expect, describe } from "vitest"
import { PostBuilder } from "../helpers/PostBuilder"
import { PostService } from "../../src/logic/service/PostService"
import { MockCollection } from "../helpers/MockCollection"
import { TaxonomyRepo } from "../../src/logic/storage/TaxonomyRepo"
import { PostRepo } from "../../src/logic/storage/PostRepo"
import type { Post } from "../../src/content/config"
import { initService } from "../helpers/di"

describe("Post Service should", async () => {
	describe("never fetch", () => {
		it("the input post", async () => {
			await assert(
				asyncProperty(setup(), async ({ relatedAmount, posts, inputPost }) => {
					const service = serviceFor(posts)

					const relatedPosts = await service.relatedTo(inputPost, relatedAmount)

					expect(relatedPosts).not.toContainEqual(inputPost)
				}),
			)
		})

		it("more posts than requested", async () => {
			await assert(
				asyncProperty(setup(), async ({ relatedAmount, posts, inputPost }) => {
					const service = serviceFor(posts)

					const relatedPosts = await service.relatedTo(inputPost, relatedAmount)

					expect(relatedPosts.length).toBeLessThanOrEqual(relatedAmount)
				}),
			)
		})

		it("more posts than available", async () => {
			await assert(
				asyncProperty(setup(), async ({ relatedAmount, posts, inputPost }) => {
					const service = serviceFor(posts)

					const relatedPosts = await service.relatedTo(inputPost, relatedAmount)

					expect(relatedPosts.length).toBeLessThan(posts.length)
				}),
			)
		})

		function setup() {
			return record({
				posts: uniqueArray(PostBuilder.asArbitrary(), { minLength: 1 }),
			}).chain(({ posts }) =>
				record({
					relatedAmount: integer({ min: 0 }),
					posts: constant(posts),
					inputPost: constantFrom(...posts),
				}),
			)
		}
	})

	describe("always fetch", async () => {
		const minAmount = PostService.minRelated

		it("the requested amount when available", async () => {
			await assert(
				asyncProperty(
					record({
						posts: validPostsArray(),
					}).chain(({ posts }) =>
						record({
							relatedAmount: integer({ min: minAmount, max: posts.length - 1 }),
							posts: constant(posts),
							inputPost: constantFrom(...posts),
						}),
					),

					async ({ relatedAmount, posts, inputPost }) => {
						const service = serviceFor(posts)

						const relatedPosts = await service.relatedTo(inputPost, relatedAmount)

						expect(relatedPosts.length).toBe(relatedAmount)
					},
				),
			)
		})

		it("the available amount when requested more", async () => {
			await assert(
				asyncProperty(
					record({
						posts: validPostsArray(),
					}).chain(({ posts }) =>
						record({
							relatedAmount: integer({ min: posts.length - 1 }),
							posts: constant(posts),
							inputPost: constantFrom(...posts),
						}),
					),

					async ({ relatedAmount, posts, inputPost }) => {
						const service = serviceFor(posts)

						const relatedPosts = await service.relatedTo(inputPost, relatedAmount)

						expect(relatedPosts.length).toBe(posts.length - 1)
					},
				),
			)
		})

		function validPostsArray() {
			return uniqueArray(PostBuilder.asArbitrary(), {
				minLength: minAmount + 1,
				comparator: (a, b) => a.data.published.getTime() === b.data.published.getTime(),
			})
		}
	})
})

function serviceFor(posts: Post[]) {
	const collection = MockCollection.Returning(posts)
	return initService(PostRepo.for(collection), TaxonomyRepo.for(collection))
}
