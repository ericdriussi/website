import { describe, it, expect } from "vitest"
import { RelatedPostsMerger } from "../../src/logic/service/RelatedPostsMerger"
import { PostBuilder } from "../helpers/PostBuilder"
import { performance } from "perf_hooks"

describe.skip("Performance for RelatedPostsMerger should be reasonable for", () => {
	it("small amounts", () => {
		const amount = 10
		const next = postArray(amount)
		const prev = postArray(amount)
		const bySeries = postArray(amount)
		const byCategory = postArray(amount)
		const byTags = postArray(amount)

		const avgTime = averageExecutionTime(() =>
			RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags),
		)

		expect(avgTime).toBeLessThan(0.11)
	})

	it("medium amounts", () => {
		const amount = 100
		const next = postArray(amount)
		const prev = postArray(amount)
		const bySeries = postArray(amount)
		const byCategory = postArray(amount)
		const byTags = postArray(amount)

		const avgTime = averageExecutionTime(() =>
			RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags),
		)

		expect(avgTime).toBeLessThan(0.34)
	})

	it("large amounts", () => {
		const amount = 1000
		const next = postArray(amount)
		const prev = postArray(amount)
		const bySeries = postArray(amount)
		const byCategory = postArray(amount)
		const byTags = postArray(amount)

		const avgTime = averageExecutionTime(() =>
			RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags),
		)

		expect(avgTime).toBeLessThan(10.7)
	})
})

function postArray(amount: number) {
	return Array.from({ length: amount }, () => PostBuilder.randomPost())
}

function averageExecutionTime(fn: () => void, iterations = 100) {
	let totalTime = 0
	for (let i = 0; i < iterations; i++) {
		const start = performance.now()
		fn()
		totalTime += performance.now() - start
	}
	return totalTime / iterations
}
