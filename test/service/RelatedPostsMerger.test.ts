import { describe, expect, it } from "vitest"
import { PostBuilder } from "../helpers/PostBuilder"
import { RelatedPostsMerger } from "../../src/logic/service/RelatedPostsMerger"
import type { Post } from "../../src/content/config"

describe("RelatedPostsMerger should", () => {
	it.each([
		{
			amount: 2,
			next: [aPost()],
			prev: [aPost()],
			bySeries: [],
			byCategory: [],
			byTags: [],
		},
		{
			amount: 3,
			next: [aPost()],
			prev: [aPost()],
			bySeries: [aPost()],
			byCategory: [],
			byTags: [],
		},
		{
			amount: 4,
			next: [aPost()],
			prev: [aPost()],
			bySeries: [aPost()],
			byCategory: [aPost()],
			byTags: [],
		},
		{
			amount: 5,
			next: [aPost()],
			prev: [aPost()],
			bySeries: [aPost()],
			byCategory: [aPost()],
			byTags: [aPost()],
		},
	])("merge $amount related posts", ({ amount, next, prev, bySeries, byCategory, byTags }) => {
		const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

		expect(result).toStrictEqual([...next, ...prev, ...bySeries, ...byCategory, ...byTags])
	})

	const amount = 5

	describe("when missing posts by date", () => {
		it("no next post", () => {
			const next: Post[] = []
			const prev = postArray(amount)
			const bySeries = postArray(amount)
			const byCategory = postArray(amount)
			const byTags = postArray(amount)

			const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

			expect(result).toStrictEqual([prev[0], prev[1], bySeries[0], byCategory[0], byTags[0]])
		})

		it("no prev post", () => {
			const next = postArray(amount)
			const prev: Post[] = []
			const bySeries = postArray(amount)
			const byCategory = postArray(amount)
			const byTags = postArray(amount)

			const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

			expect(result).toStrictEqual([next[0], next[1], bySeries[0], byCategory[0], byTags[0]])
		})
	})

	describe("when missing more relevant posts, fill with posts", () => {
		it("by series", () => {
			const next: Post[] = []
			const prev: Post[] = []
			const bySeries = postArray(amount)
			const byCategory = postArray(amount)
			const byTags = postArray(amount)

			const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

			expect(result).toStrictEqual([bySeries[0], bySeries[1], byCategory[0], byCategory[1], byTags[0]])
		})

		it("by category", () => {
			const next: Post[] = []
			const prev: Post[] = []
			const bySeries: Post[] = []
			const byCategory = postArray(amount)
			const byTags = postArray(amount)

			const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

			expect(result).toStrictEqual([byCategory[0], byCategory[1], byTags[0], byTags[1], byTags[2]])
		})

		it("by tags", () => {
			const next: Post[] = []
			const prev: Post[] = []
			const bySeries: Post[] = []
			const byCategory: Post[] = []
			const byTags = postArray(amount)

			const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

			expect(result).toStrictEqual([byTags[0], byTags[1], byTags[2], byTags[3], byTags[4]])
		})

		it("by date, category, and tags", () => {
			const next = postArray(amount)
			const prev = postArray(amount)
			const bySeries: Post[] = []
			const byCategory = postArray(amount)
			const byTags = postArray(amount)

			const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

			expect(result).toStrictEqual([next[0], prev[0], byCategory[0], byCategory[1], byTags[0]])
		})
	})
})

function aPost() {
	return PostBuilder.randomPost()
}

function postArray(amount: number) {
	return Array.from({ length: amount }, () => aPost())
}
