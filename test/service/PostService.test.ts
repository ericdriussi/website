import "../helpers/date.d.ts" // add utility functions to Date prototype
import { describe, it, expect } from "vitest"
import { PostBuilder } from "../helpers/PostBuilder.ts"
import { MockPostRepo } from "../helpers/MockPostRepo.ts"
import { MockTaxonomyRepo } from "../helpers/MockTaxonomyRepo.ts"
import { initService } from "../helpers/di.ts"

describe("Post Service should", async () => {
	const today = new Date()

	it("sort posts by newest", async () => {
		const todaysPost = new PostBuilder().withPublished(today).build()
		const yesterdaysPost = new PostBuilder().withPublished(today.aDayLess()).build()
		const oldestPost = new PostBuilder().withPublished(today.aDayLess().aDayLess()).build()
		const posts = [oldestPost, todaysPost, yesterdaysPost]
		const postRepo = MockPostRepo.returning(posts)
		// @ts-ignore
		const postService = initService(postRepo, MockTaxonomyRepo.empty())

		const sortedPosts = await postService.allSorted()

		expect(sortedPosts).toStrictEqual([todaysPost, yesterdaysPost, oldestPost])
	})
})
