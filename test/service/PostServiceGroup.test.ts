import "../helpers/date.d.ts" // add utility functions to Date prototype
import { initService } from "../helpers/di"
import { describe, expect, it } from "vitest"
import { PostBuilder } from "../helpers/PostBuilder.ts"
import { MockPostRepo } from "../helpers/MockPostRepo.ts"
import { MockTaxonomyRepo } from "../helpers/MockTaxonomyRepo.ts"

describe("Post Service should group", async () => {
	describe("by year", async () => {
		const year1970 = new Date(1970, 1, 1)
		const year1971 = new Date(1971, 1, 1)

		it("sorting groups by newest", async () => {
			const postIn1970 = new PostBuilder().withPublished(year1970).build()
			const postIn1971 = new PostBuilder().withPublished(year1971).build()
			const postRepo = MockPostRepo.returning([postIn1970, postIn1971])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const groupedPosts = await postService.byYear()

			expect(groupedPosts).toStrictEqual([
				{ year: year1971.getFullYear(), posts: [postIn1971] },
				{ year: year1970.getFullYear(), posts: [postIn1970] },
			])
		})

		it("sorting posts by newest", async () => {
			const olderPostIn1970 = new PostBuilder().withPublished(year1970).build()
			const newerPostIn1970 = new PostBuilder().withPublished(year1970.aDayMore()).build()
			const postRepo = MockPostRepo.returning([olderPostIn1970, newerPostIn1970])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const groupedPosts = await postService.byYear()

			expect(groupedPosts).toStrictEqual([
				{ year: year1970.getFullYear(), posts: [newerPostIn1970, olderPostIn1970] },
			])
		})
	})

	describe("by series", async () => {
		it("sorting groups alphabetically", async () => {
			const seriesZ = "Series Z"
			const postInSeriesZ = new PostBuilder().withSeries(seriesZ).build()
			const seriesA = "Series A"
			const postInSeriesA = new PostBuilder().withSeries(seriesA).build()
			const seriesB = "Series B"
			const postInSeriesB = new PostBuilder().withSeries(seriesB).build()
			const postRepo = MockPostRepo.returning([postInSeriesA, postInSeriesB, postInSeriesZ])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const postsBySeries = await postService.bySeries()

			expect(postsBySeries).toStrictEqual([
				{ series: seriesA, posts: [postInSeriesA] },
				{ series: seriesB, posts: [postInSeriesB] },
				{ series: seriesZ, posts: [postInSeriesZ] },
			])
		})

		it("sorting posts by newest", async () => {
			const series = "Series"
			const today = new Date()
			const todayPost = new PostBuilder().withSeries(series).withPublished(today).build()
			const yesterdayPost = new PostBuilder().withSeries(series).withPublished(today.aDayLess()).build()
			const postRepo = MockPostRepo.returning([yesterdayPost, todayPost])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const postsBySeries = await postService.bySeries()

			expect(postsBySeries).toStrictEqual([{ series, posts: [todayPost, yesterdayPost] }])
		})

		it("discarding invalid series", async () => {
			const series = "Valid Series"
			const postWithSeries = new PostBuilder().withSeries(series).build()
			const postWithoutSeries = new PostBuilder().build()
			const postWithEmptySeries = new PostBuilder().withSeries("").build()
			const postRepo = MockPostRepo.returning([postWithSeries, postWithoutSeries, postWithEmptySeries])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const postsBySeries = await postService.bySeries()

			expect(postsBySeries).toStrictEqual([{ series, posts: [postWithSeries] }])
		})
	})

	describe("by categories", async () => {
		it("sorting groups alphabetically", async () => {
			const categoryZ = "Category Z"
			const postInCategoryZ = new PostBuilder().withCategory(categoryZ).build()
			const categoryA = "Category A"
			const postInCategoryA = new PostBuilder().withCategory(categoryA).build()
			const categoryB = "Category B"
			const postInCategoryB = new PostBuilder().withCategory(categoryB).build()
			const postRepo = MockPostRepo.returning([postInCategoryA, postInCategoryB, postInCategoryZ])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const postsByCategory = await postService.byCategories()

			expect(postsByCategory).toStrictEqual([
				{ category: categoryA, posts: [postInCategoryA] },
				{ category: categoryB, posts: [postInCategoryB] },
				{ category: categoryZ, posts: [postInCategoryZ] },
			])
		})

		it("sorting posts by newest", async () => {
			const category = "Category"
			const today = new Date()
			const todayPost = new PostBuilder().withCategory(category).withPublished(today).build()
			const yesterdayPost = new PostBuilder().withCategory(category).withPublished(today.aDayLess()).build()
			const postRepo = MockPostRepo.returning([yesterdayPost, todayPost])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const postsByCategory = await postService.byCategories()

			expect(postsByCategory).toStrictEqual([{ category, posts: [todayPost, yesterdayPost] }])
		})

		it("discarding invalid categories", async () => {
			const category = "Valid Category"
			const postWithCategory = new PostBuilder().withCategory(category).build()
			const postWithoutCategory = new PostBuilder().build()
			const postWithEmptyCategory = new PostBuilder().withCategory("").build()
			const postRepo = MockPostRepo.returning([postWithCategory, postWithoutCategory, postWithEmptyCategory])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const postsByCategory = await postService.byCategories()

			expect(postsByCategory).toStrictEqual([{ category, posts: [postWithCategory] }])
		})
	})

	describe("by tags", async () => {
		it("sorting groups alphabetically", async () => {
			const tagZ = "Tag Z"
			const postWithTagZ = new PostBuilder().withTags([tagZ]).build()
			const tagA = "Tag A"
			const postWithTagA = new PostBuilder().withTags([tagA]).build()
			const tagB = "Tag B"
			const postWithTagB = new PostBuilder().withTags([tagB]).build()
			const postRepo = MockPostRepo.returning([postWithTagA, postWithTagB, postWithTagZ])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const postsByTag = await postService.byTags()

			expect(postsByTag).toStrictEqual([
				{ tag: tagA, posts: [postWithTagA] },
				{ tag: tagB, posts: [postWithTagB] },
				{ tag: tagZ, posts: [postWithTagZ] },
			])
		})

		it("sorting posts by newest", async () => {
			const tag = "Tag"
			const today = new Date()
			const todayPost = new PostBuilder().withTags([tag]).withPublished(today).build()
			const yesterdayPost = new PostBuilder().withTags([tag]).withPublished(today.aDayLess()).build()
			const postRepo = MockPostRepo.returning([yesterdayPost, todayPost])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const postsByTag = await postService.byTags()

			expect(postsByTag).toStrictEqual([{ tag, posts: [todayPost, yesterdayPost] }])
		})

		it("discarding invalid tags", async () => {
			const tag = "Valid Tag"
			const postWithTag = new PostBuilder().withTags([tag]).build()
			const postWithoutTag = new PostBuilder().build()
			const postWithEmptyTag = new PostBuilder().withTags([]).build()
			const postRepo = MockPostRepo.returning([postWithTag, postWithoutTag, postWithEmptyTag])
			// @ts-ignore
			const postService = initService(postRepo, MockTaxonomyRepo.empty())

			const postsByTag = await postService.byTags()

			expect(postsByTag).toStrictEqual([{ tag, posts: [postWithTag] }])
		})
	})
})
