import "../helpers/date.d.ts" // add utility functions to Date prototype
import { faker } from "@faker-js/faker"
import { describe, expect, it } from "vitest"
import { PostBuilder } from "../helpers/PostBuilder"
import { PostSorter } from "../../src/logic/service/PostSorter"
import type { Post } from "../../src/content/config"

describe("PostSorter should", () => {
	const series = faker.lorem.word()
	const category = faker.lorem.word()
	const tags = [faker.lorem.word(), faker.lorem.word()]
	const referencePost = new PostBuilder().withSeries(series).withCategory(category).withTags(tags).build()

	describe("sort posts", () => {
		it("by similarity based on series, category, and tags", () => {
			const verySimilar = new PostBuilder().withSeries(series).withCategory(category).withTags(tags).build()
			const similarSeries = new PostBuilder().withSeries(series).build()
			const similarCategory = new PostBuilder().withCategory(category).build()
			const similarTags = new PostBuilder().withTags(tags).build()
			const similarTag = new PostBuilder().withTags([tags[0]]).build()
			const nothingInCommon = new PostBuilder().build()

			const sorted = PostSorter.bySimilarity(
				referencePost,
				shuffled([nothingInCommon, similarTag, similarTags, similarCategory, similarSeries, verySimilar]),
			)

			expect(sorted).toEqual([
				verySimilar,
				similarSeries,
				similarCategory,
				similarTags,
				similarTag,
				nothingInCommon,
			])
		})

		describe("by date", () => {
			it("newest first", () => {
				const today = new Date()
				const newPost = new PostBuilder().withPublished(today).build()
				const oldPost = new PostBuilder().withPublished(today.aDayLess()).build()

				const sorted = PostSorter.newestFirts([oldPost, newPost])

				expect(sorted).toEqual([newPost, oldPost])
			})

			it("oldest first", () => {
				const today = new Date()
				const newPost = new PostBuilder().withPublished(today).build()
				const oldPost = new PostBuilder().withPublished(today.aDayLess()).build()

				const sorted = PostSorter.oldestFirst([newPost, oldPost])

				expect(sorted).toEqual([oldPost, newPost])
			})
		})
	})

	describe("not sort posts", () => {
		it("when no similarities are found", () => {
			const posts = [new PostBuilder().build(), new PostBuilder().build()]

			const sorted = PostSorter.bySimilarity(referencePost, posts)

			expect(sorted).toEqual(posts)
		})
	})
})

function shuffled(array: Post[]): Post[] {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1))
		;[array[i], array[j]] = [array[j], array[i]]
	}
	return array
}
