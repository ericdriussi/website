import { assert, asyncProperty, integer, record, uniqueArray } from "fast-check"
import { describe, it, expect } from "vitest"
import { RelatedPostsMerger } from "../../src/logic/service/RelatedPostsMerger"
import { PostBuilder } from "../helpers/PostBuilder"
import type { Post } from "../../src/content/config"
import { uiConfig } from "../../src/consts"

describe("RelatedPostsMerger should", () => {
	it("not merge more posts than the requested amount", async () => {
		await assert(
			asyncProperty(
				integer({ min: 0 }),
				uniqueArray(PostBuilder.asArbitrary()),
				uniqueArray(PostBuilder.asArbitrary()),
				uniqueArray(PostBuilder.asArbitrary()),
				uniqueArray(PostBuilder.asArbitrary()),
				uniqueArray(PostBuilder.asArbitrary()),

				async (amount, next, prev, bySeries, byCategory, byTags) => {
					const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

					expect(result.length).toBeLessThanOrEqual(amount)
				},
			),
		)
	})

	it("not merge duplicates", async () => {
		await assert(
			asyncProperty(
				integer(),
				uniqueArray(PostBuilder.asArbitrary()),
				uniqueArray(PostBuilder.asArbitrary()),
				uniqueArray(PostBuilder.asArbitrary()),
				uniqueArray(PostBuilder.asArbitrary()),
				uniqueArray(PostBuilder.asArbitrary()),

				async (amount, next, prev, bySeries, byCategory, byTags) => {
					const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

					expect(result.length).toBe(new Set(result).size)
				},
			),
		)
	})

	it("evenly divide amount of posts related by date between next and prev slot", async () => {
		await assert(
			asyncProperty(
				integer({ min: 2 }),
				uniqueArray(PostBuilder.asArbitrary(), { minLength: 1 }),
				uniqueArray(PostBuilder.asArbitrary(), { minLength: 1 }),
				uniqueArray(PostBuilder.asArbitrary()),
				uniqueArray(PostBuilder.asArbitrary()),
				uniqueArray(PostBuilder.asArbitrary()),

				async (amount, next, prev, bySeries, byCategory, byTags) => {
					const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

					const nextInResult = result.filter((post) => next.includes(post))
					const prevInResult = result.filter((post) => prev.includes(post))

					const expectedNext = expectedLength(amount, next)
					const expectedPrev = expectedLength(amount, prev)

					expect(nextInResult.length).toBe(expectedNext)
					expect(prevInResult.length).toBe(expectedPrev)
				},
			),
		)

		function expectedLength(amount: number, arr: Post[]) {
			const adjustedAmount = Math.floor(amount / 5)
			const maxDate = Math.max(2, adjustedAmount * 2)
			return Math.min(amount, arr.length, Math.floor(maxDate / 2))
		}
	})

	describe("merge nothing when", async () => {
		it("given empty lists", async () => {
			await assert(
				asyncProperty(integer(), async (amount) => {
					const result = RelatedPostsMerger.merge(amount, [], [], [], [], [])

					expect(result).toHaveLength(0)
				}),
			)
		})

		it("amount is less than min", async () => {
			await assert(
				asyncProperty(
					integer({ max: RelatedPostsMerger.minAmount - 1 }),
					uniqueArray(PostBuilder.asArbitrary()),
					uniqueArray(PostBuilder.asArbitrary()),
					uniqueArray(PostBuilder.asArbitrary()),
					uniqueArray(PostBuilder.asArbitrary()),
					uniqueArray(PostBuilder.asArbitrary()),

					async (amount, next, prev, bySeries, byCategory, byTags) => {
						const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

						expect(result).toHaveLength(0)
					},
				),
			)
		})
	})

	it("merge posts when enough are provided", async () => {
		const maxAmount = 25 // Perf, takes forever if removed

		await assert(
			asyncProperty(
				record({
					amount: integer({ min: uiConfig.relatedPostsFactor, max: maxAmount }),
				}).chain(({ amount }) => {
					const totalAmount = amount * 5 // 5 arrays passed to SUT

					// This is done to ensure no duplicated posts are in any of the arrays
					// Needed for the assertion to make sense
					return uniqueArray(PostBuilder.asArbitrary(), {
						minLength: totalAmount,
						maxLength: totalAmount,
						comparator: (a, b) => a.data.title === b.data.title,
					}).map((posts) => {
						return {
							amount,
							...splitPosts(posts, amount),
						}
					})
				}),

				async ({ amount, next, prev, bySeries, byCategory, byTags }) => {
					const result = RelatedPostsMerger.merge(amount, next, prev, bySeries, byCategory, byTags)

					const amountOfEach = Math.floor(amount / uiConfig.relatedPostsFactor)
					const remainingSlots = amount - amountOfEach * 4 // amountOfEach * num of previously merged arrays
					expect(result).toStrictEqual([
						...next.slice(0, amountOfEach),
						...prev.slice(0, amountOfEach),
						...bySeries.slice(0, amountOfEach),
						...byCategory.slice(0, amountOfEach),
						...byTags.slice(0, remainingSlots),
					])
				},
			),
		)

		function splitPosts(posts: Post[], amount: number) {
			return {
				next: posts.slice(0, amount),
				prev: posts.slice(amount, amount * 2),
				bySeries: posts.slice(amount * 2, amount * 3),
				byCategory: posts.slice(amount * 3, amount * 4),
				byTags: posts.slice(amount * 4),
			}
		}
	})
})
