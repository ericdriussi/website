import { describe, it, expect, vi, beforeEach, afterEach } from "vitest"
import matter from "gray-matter"
import { DraftBuilder } from "./DraftBuilder"
import { Publisher } from "../../../src/logic/hooks/build_start/Publisher"

describe("Publisher should", () => {
	const mockFs = fsMock()
	const mockMatter = grayMatterMock()
	const mockPath = { join: vi.fn().mockImplementation((...paths: string[]) => paths.join("/")) }

	const baseDir = "irrelevant/dir"
	const draftsDirectory = baseDir.concat("/src/content/draft")
	const postsDirectory = baseDir.concat("/src/content/blog")
	let publisher: Publisher

	beforeEach(() => {
		publisher = new Publisher({ fs: mockFs, path: mockPath, matter: mockMatter, baseDir: baseDir })
	})

	afterEach(() => {
		clearMocks()
	})

	it("read from drafts directory", () => {
		const aFile = new DraftBuilder().build()
		mockFs.readdirSync.mockReturnValue([aFile.name])
		mockFs.statSync.mockReturnValue({ mtime: aFile.mtime })
		mockMatter.parse.mockReturnValue({ data: aFile.data, content: aFile.content })

		publisher.publishDrafts()

		expect(mockFs.readdirSync).toHaveBeenCalledWith(draftsDirectory)
	})

	it("read drafts sorted by modification time", () => {
		const [file1, file2, file3] = DraftBuilder.NDraftsByMtime(3)
		mockFs.readdirSync.mockReturnValue([file1.name, file2.name, file3.name])
		mockFs.statSync.mockImplementation((filePath: string) => {
			if (filePath.includes(file1.name)) {
				return { mtime: file1.mtime }
			}
			if (filePath.includes(file2.name)) {
				return { mtime: file2.mtime }
			}
			if (filePath.includes(file3.name)) {
				return { mtime: file3.mtime }
			}
			throw new Error(`Unexpected argument: ${filePath}`)
		})
		mockMatter.parse.mockReturnValue({ data: {}, content: "irrelevant" })

		publisher.publishDrafts()

		expect(mockFs.readFileSync.mock.calls[0][0]).toContain(file1.name)
		expect(mockFs.readFileSync.mock.calls[1][0]).toContain(file2.name)
		expect(mockFs.readFileSync.mock.calls[2][0]).toContain(file3.name)
	})

	it("publish a draft that is ready", () => {
		const isReadyData = { is_ready: true }
		const readyDraft = new DraftBuilder().withData(isReadyData).build()

		mockFs.readdirSync.mockReturnValue([readyDraft.name])
		mockFs.statSync.mockReturnValue({ mtime: readyDraft.mtime })
		mockFs.readFileSync.mockReturnValue(readyDraft.content)
		mockMatter.parse.mockReturnValue({ data: readyDraft.data, content: readyDraft.content })
		const currDate = new Date().toLocaleDateString("en-GB", { day: "2-digit", month: "short", year: "numeric" })
		const contentToPublish = matter.stringify(readyDraft.content, { ...isReadyData, published: currDate })
		mockMatter.stringify.mockImplementation((content: string, data: object) => {
			if (content === readyDraft.content && data === isReadyData) {
				return contentToPublish
			}
			throw new Error(`Unexpected argument: ${content}`)
		})

		publisher.publishDrafts()

		expect(isReadyData).not.toHaveProperty("is_ready")
		expect(mockFs.writeFileSync).toHaveBeenCalledWith(draftPathOf(readyDraft.name), contentToPublish)
		expect(mockFs.renameSync).toHaveBeenCalledWith(draftPathOf(readyDraft.name), postPathOf(readyDraft.name))
	})

	it("not publish a draft that is not ready", () => {
		const isNotReadyData = { is_ready: false }
		const notReadyDraft = new DraftBuilder().withData(isNotReadyData).build()

		mockFs.readdirSync.mockReturnValue([notReadyDraft.name])
		mockFs.statSync.mockReturnValue({ mtime: notReadyDraft.mtime })
		mockFs.readFileSync.mockReturnValue(notReadyDraft.content)
		mockMatter.parse.mockReturnValue({ data: notReadyDraft.data, content: notReadyDraft.content })

		publisher.publishDrafts()

		expect(mockMatter.stringify).not.toHaveBeenCalled()
		expect(mockFs.writeFileSync).not.toHaveBeenCalled()
		expect(mockFs.renameSync).not.toHaveBeenCalled()
	})

	it("only publish the oldest draft when more than one is ready", () => {
		const isReadyData = { is_ready: true }
		const readyDraft1 = new DraftBuilder()
			.withName("draft1")
			.withMtime(new Date(1970, 1, 1))
			.withData(isReadyData)
			.build()
		const readyDraft2 = new DraftBuilder()
			.withName("draft2")
			.withMtime(new Date(1970, 1, 2))
			.withData(isReadyData)
			.build()

		mockFs.readdirSync.mockReturnValue([readyDraft1.name, readyDraft2.name])
		mockFs.statSync.mockImplementation((filePath: string) => {
			if (filePath.includes(readyDraft1.name)) {
				return { mtime: readyDraft1.mtime }
			}
			if (filePath.includes(readyDraft2.name)) {
				return { mtime: readyDraft2.mtime }
			}
			throw new Error(`Unexpected argument: ${filePath}`)
		})
		mockFs.readFileSync.mockReturnValue("irrelevant")
		mockMatter.parse.mockReturnValue({ data: isReadyData, content: "irrelevant" })
		const currDate = new Date().toLocaleDateString("en-GB", { day: "2-digit", month: "short", year: "numeric" })
		const contentToPublish = matter.stringify(readyDraft1.content, { ...isReadyData, published: currDate })
		mockMatter.stringify.mockReturnValue(contentToPublish)

		publisher.publishDrafts()

		expect(mockFs.writeFileSync).toHaveBeenCalledOnce()
		expect(mockFs.writeFileSync).toHaveBeenCalledWith(draftPathOf(readyDraft1.name), contentToPublish)
		expect(mockFs.renameSync).toHaveBeenCalledOnce()
		expect(mockFs.renameSync).toHaveBeenCalledWith(draftPathOf(readyDraft1.name), postPathOf(readyDraft1.name))
	})

	function draftPathOf(draftName: string): string {
		return draftsDirectory.concat("/", draftName)
	}

	function postPathOf(postName: string): string {
		return postsDirectory.concat("/", postName)
	}

	function clearMocks() {
		mockFs.readdirSync.mockReset()
		mockFs.statSync.mockReset()
		mockFs.readFileSync.mockReset()
		mockFs.renameSync.mockReset()
		mockFs.writeFileSync.mockReset()
		mockMatter.parse.mockReset()
		mockMatter.stringify.mockReset()
	}

	function grayMatterMock() {
		return {
			parse: vi.fn(),
			stringify: vi.fn(),
		}
	}

	function fsMock() {
		return {
			readdirSync: vi.fn(),
			readFileSync: vi.fn(),
			statSync: vi.fn(),
			writeFileSync: vi.fn(),
			renameSync: vi.fn(),
		}
	}
})
