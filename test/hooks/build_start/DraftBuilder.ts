export class DraftBuilder {
	private draft: { name: string; mtime: Date; data: {}; content: string }

	constructor() {
		this.draft = {
			name: "default_name.md",
			mtime: new Date(1970, 1, 1),
			data: {},
			content: "default content",
		}
	}

	withName(name: string): DraftBuilder {
		this.draft.name = name
		return this
	}

	withMtime(mtime: Date): DraftBuilder {
		this.draft.mtime = mtime
		return this
	}

	withData(data: {}): DraftBuilder {
		this.draft.data = data
		return this
	}

	withContent(content: string): DraftBuilder {
		this.draft.content = content
		return this
	}

	build(): { name: string; mtime: Date; data: {}; content: string } {
		return this.draft
	}

	static NDraftsByMtime(n: number): { name: string; mtime: Date; data: {}; content: string }[] {
		return Array.from({ length: n }, (_, i) => {
			return new DraftBuilder()
				.withName(`file${i + 1}.md`)
				.withMtime(new Date(1970, 1, i + 1))
				.withContent(`content ${i + 1}`)
				.build()
		})
	}
}
