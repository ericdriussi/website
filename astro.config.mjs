import { defineConfig } from "astro/config"
import sitemap from "@astrojs/sitemap"
import { dev, site } from "./src/consts"
import icon from "astro-icon"
import { pluginLineNumbers } from "@expressive-code/plugin-line-numbers"
import expressiveCode from "astro-expressive-code"
import { publishOldestReadyDraft } from "./src/logic/hooks/build_start/PublishHook"
import { imagePathFormatter } from "./src/logic/md_pre/FormatImagePaths"
import { h1HeadingsRemover } from "./src/logic/md_pre/RemoveH1Headings"
import gruvbox from "gruvbox-material/themes/gruvbox-material-dark.json"
import rehypeSlug from "rehype-slug"
import rehypeAutolinkHeadings from "rehype-autolink-headings"
import { codeBlocksFormatter } from "./src/logic/md_pre/FormatCodeBlocks"
import remarkBreaks from "remark-breaks"
import pagefind from "astro-pagefind"

export default defineConfig({
	site: site.url,
	server: { port: dev.serverPort },
	markdown: {
		remarkPlugins: [imagePathFormatter(), h1HeadingsRemover(), codeBlocksFormatter(), remarkBreaks],

		rehypePlugins: [
			rehypeSlug,
			[
				rehypeAutolinkHeadings,
				{
					behavior: "wrap",
				},
			],
		],
	},
	integrations: [
		{
			name: "publish-oldest-ready-draft",
			hooks: publishOldestReadyDraft(),
		},
		sitemap(),
		icon({ iconDir: "src/assets" }),
		expressiveCode({
			plugins: [pluginLineNumbers()],
			themes: [gruvbox],
			styleOverrides: {
				codeFontFamily: "jetbrains-mono",
				uiFontFamily: "jetbrains-mono",
			},
		}),
		pagefind(),
	],
})
