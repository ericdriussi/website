[pub]
alias = "p"
description = "Publish site"
depends = ["install", "lint", "test:unit", "build --prod"]
depends_post = ["upload"]

["pub:ci"]
depends = ["install", "lint", "test:unit"]
run = """
latest_tag=$(git tag | sort -V | tail -n 1)
changes_since_pub=$(git diff --name-only "$latest_tag" HEAD -- .)

tests_or_drafts="^(test|src/content/draft)"
dev_conf_files="^(\\.editorconfig|\\.gitignore|\\.markdownlint|\\.prettierrc|eslint|vitest)"
number_of_relevant_changes=$(echo "$changes_since_pub" | grep -Ev "$tests_or_drafts" | grep -Ev "$dev_conf_files" | wc -l)
if [ "$number_of_relevant_changes" -lt 1 ]; then
    echo "Nothing to publish"
    exit 0
fi

blog_posts="^src/content/blog/"
number_of_posts_since_pub=$(echo "$changes_since_pub" | grep "$blog_posts" | wc -l)
if [ "$number_of_posts_since_pub" -gt 10 ]; then
    echo -e "More than 10 posts were modified/created since last publish: Manual publish required"
    exit 0
fi

# Not in depends_post so they only run if did not exit 0
mise run build --prod
mise run upload
"""

["pub:auto"]
depends = ["install", "build --autopub"]
depends_post = ["upload"]
run = """
mise run lint && mise run test:unit
# Not in depends to ensure the new post is OK

new_posts=$(git ls-files --others --exclude-standard ./src/content/blog/)
number_of_new_posts=$(echo "$new_posts" | grep . | wc -l)

if [ "$number_of_new_posts" -gt 1 ]; then
    printf "[ERROR] More than one post was queued:\n"
    echo "$new_posts"
    exit 1
fi

if [ "$number_of_new_posts" -lt 1 ]; then
    echo "No new post to publish"
    exit 0
fi

git add .
commit_message="[CI] Autopub post\n$new_posts"
git commit -m "$commit_message" || true
"""

[upload]
hide = true
run = """
set -e

if [ "$(git describe --tags --exact-match 2>/dev/null)" ]; then
	# If latest tag points to HEAD
	echo "Nothing to upload"
	exit 0
fi

latest_tag=$(git tag | sort -V | tail -n 1)
latest_tag_number=$(echo "$latest_tag" | sed 's/[^0-9]//g')
new_tag_number=$((latest_tag_number + 1))
new_tag="v$new_tag_number"

git tag -a "$new_tag" -m "$(date +"%Y-%m-%d")"
# Ensure the tag is pushed even if no new commit is made
git push -o ci.skip origin "$new_tag"
# Push new commits if any
git push -o ci.skip origin HEAD:master

echo "Deploying..."
# TODO: Use rsync once the server is properly set-up
scp -r ./dist/* deploy:~/deploy_blog
printf "Published tag %s\n" "$new_tag"
"""
