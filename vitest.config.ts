import { defineConfig } from "vitest/config"

export default defineConfig({
	test: {
		environmentMatchGlobs: [["test/view/**", "happy-dom"]],
		exclude: ["test/e2e/**"],
		include: ["test/**/*.test.ts"],
	},
})
