import { defineConfig, devices } from "@playwright/test"
import { dev } from "./src/consts"

export default defineConfig({
	testDir: "./test/e2e/",
	fullyParallel: true,
	/* Fail the build on CI if you accidentally left test.only in the source code. */
	forbidOnly: !!process.env.CI,
	/* Retry on CI only */
	retries: process.env.CI ? 2 : 0,
	/* Opt out of parallel tests on CI. */
	workers: process.env.CI ? 1 : undefined,

	outputDir: "playwright/results",
	reporter: [["html", { open: "never", outputFolder: "playwright/report" }]],

	use: {
		baseURL: `http://localhost:${dev.serverPort}/`,
		trace: "on-first-retry",
		launchOptions: {
			args: [`--remote-debugging-port=${dev.e2eDebugPort}`],
		},
	},

	projects: [
		{ name: "chromium", use: { ...devices["Desktop Chrome"] } },
		// { name: "firefox", use: { ...devices["Desktop Firefox"] } },
		// { name: "Mobile Pixel", use: { ...devices["Pixel 5"] } },
	],

	webServer: {
		command: "mise run build --serve",
		url: `http://localhost:${dev.serverPort}/`,
		timeout: 120 * 1000,
		reuseExistingServer: !process.env.CI,
	},
})
