import type { Post } from "../../content/config"

export class PostSorter {
	static SERIES_WEIGHT = 100
	static CATEGORY_WEIGHT = 50
	static TAG_WEIGHT = 5

	static newestFirts(posts: Post[]): Post[] {
		// ? is used to support drafts
		return posts.sort((a, b) => b.data.published?.getTime() - a.data.published?.getTime())
	}

	static oldestFirst(posts: Post[]): Post[] {
		// ? is used to support drafts
		return posts.sort((a, b) => a.data.published?.getTime() - b.data.published?.getTime())
	}

	static bySimilarity(referencePost: Post, posts: Post[]): Post[] {
		return posts
			.map((post) => ({
				post: post,
				weight: this.calculateWeight(referencePost, post),
			}))
			.sort((a, b) => b.weight - a.weight)
			.map((item) => item.post)
	}

	private static calculateWeight(refPost: Post, otherPost: Post): number {
		const seriesWeight = refPost.data.series === otherPost.data.series ? this.SERIES_WEIGHT : 0
		const categoryWeight = refPost.data.category === otherPost.data.category ? this.CATEGORY_WEIGHT : 0

		const tagWeight = (refPost.data.tags ?? []).reduce((score, tag) => {
			return otherPost.data.tags?.includes(tag) ? score + this.TAG_WEIGHT : score
		}, 0)

		return seriesWeight + categoryWeight + tagWeight
	}
}
