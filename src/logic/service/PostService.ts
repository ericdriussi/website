import type { Post } from "../../content/config"
import { PostSorter } from "./PostSorter"
import { RelatedPostsMerger } from "./RelatedPostsMerger"
import type { PostRepo } from "../storage/PostRepo"
import type { TaxonomyRepo } from "../storage/TaxonomyRepo"

type PostsByYears = { year: number; posts: Post[] }[]
type PostsByCategories = { category: string; posts: Post[] }[]
type PostsByTags = { tag: string; posts: Post[] }[]
type PostsBySeries = { series: string; posts: Post[] }[]

export class PostService {
	public static readonly minRelated = RelatedPostsMerger.minAmount
	private postRepo: PostRepo
	private taxonomyRepo: TaxonomyRepo

	constructor(postRepo: PostRepo, taxonomyRepo: TaxonomyRepo) {
		this.postRepo = postRepo
		this.taxonomyRepo = taxonomyRepo
	}

	async allSorted(): Promise<Post[]> {
		return PostSorter.newestFirts(await this.postRepo.all())
	}

	async byYear(): Promise<PostsByYears> {
		const allPosts = PostSorter.newestFirts(await this.postRepo.all())
		return allPosts.reduce<PostsByYears>((postsByYears, post) => {
			const year = new Date(post.data.published).getFullYear()
			if (!year) {
				// Filter out drafts
				return postsByYears
			}

			const currentYearGroup = postsByYears.find((postsGroup) => postsGroup.year === year)
			if (currentYearGroup) {
				// add post to existing group
				currentYearGroup.posts.push(post)
			} else {
				//or create a new group with post
				postsByYears.push({ year, posts: [post] })
			}

			return postsByYears
		}, [])
	}

	async bySeries(): Promise<PostsBySeries> {
		const allPosts = PostSorter.newestFirts(await this.postRepo.all())
		const postsBySeries = allPosts.reduce<PostsBySeries>((postsBySeries, post) => {
			const series = post.data.series
			// Skip empty or non-existent series
			if (!series) {
				return postsBySeries
			}

			const seriesGroup = postsBySeries.find((entry) => entry.series === series)
			if (seriesGroup) {
				// add post to existing group
				seriesGroup.posts.push(post)
			} else {
				//or create a new group with post
				postsBySeries.push({ series, posts: [post] })
			}
			return postsBySeries
		}, [])
		return postsBySeries.sort((a, b) => a.series.localeCompare(b.series))
	}

	async byCategories(): Promise<PostsByCategories> {
		const allPosts = PostSorter.newestFirts(await this.postRepo.all())
		const postsByCategories = allPosts.reduce<PostsByCategories>((postsByCategories, post) => {
			const category = post.data.category
			// Skip empty or non-existent categories
			if (!category) {
				return postsByCategories
			}

			const categoryGroup = postsByCategories.find((entry) => entry.category === category)
			if (categoryGroup) {
				// add post to existing group
				categoryGroup.posts.push(post)
			} else {
				//or create a new group with post
				postsByCategories.push({ category, posts: [post] })
			}
			return postsByCategories
		}, [])
		return postsByCategories.sort((a, b) => a.category.localeCompare(b.category))
	}

	async byTags(): Promise<PostsByTags> {
		const allPosts = PostSorter.newestFirts(await this.postRepo.all())
		const postsByTags = allPosts.reduce<PostsByTags>((postsByTags, post) => {
			const tags = post.data.tags
			// Skip empty or non-existent tags
			if (!tags) {
				return postsByTags
			}

			// Posts can have multiple tags
			tags.forEach((tag) => {
				const tagGroup = postsByTags.find((entry) => entry.tag === tag)
				if (tagGroup) {
					// add post to existing group
					tagGroup.posts.push(post)
				} else {
					//or create a new group with post
					postsByTags.push({ tag, posts: [post] })
				}
			})
			return postsByTags
		}, [])
		return postsByTags.sort((a, b) => a.tag.localeCompare(b.tag))
	}

	async relatedTo(post: Post, amount: number): Promise<Post[]> {
		const { postsAfter, postsBefore } = await this.relatedByDate(post)
		const { sameSeries, sameCategory, similarTags } = await this.relatedByTaxonomy(post)

		const merged = RelatedPostsMerger.merge(
			amount,
			PostSorter.oldestFirst(postsAfter),
			PostSorter.newestFirts(postsBefore),
			PostSorter.bySimilarity(post, sameSeries),
			PostSorter.bySimilarity(post, sameCategory),
			PostSorter.bySimilarity(post, similarTags),
		)

		const otherAvailablePosts = postsAfter.length + postsBefore.length - merged.length
		const desiredAmount = Math.min(otherAvailablePosts, amount - merged.length) + merged.length

		while (merged.length < desiredAmount) {
			await this.addRandomPost(merged, post)
		}

		return merged
	}

	private async relatedByDate(post: Post) {
		const postsAfter = await this.postRepo.allAfter(post.data.published)
		const postsBefore = await this.postRepo.allBefore(post.data.published)
		return { postsAfter, postsBefore }
	}

	private async relatedByTaxonomy(post: Post) {
		const sameSeries = (await this.taxonomyRepo.allInSeries(post.data.series)).filter(
			(related) => !areEqual(post, related),
		)
		const sameCategory = (await this.taxonomyRepo.allWithCategory(post.data.category)).filter(
			(related) => !areEqual(post, related),
		)
		const similarTags = (await this.taxonomyRepo.allWithSomeOf(post.data.tags)).filter(
			(related) => !areEqual(post, related),
		)

		return { sameSeries, sameCategory, similarTags }
	}

	private async addRandomPost(merged: Post[], post: Post) {
		const random = await this.postRepo.random()
		if (!merged.includes(random) && !areEqual(post, random)) {
			merged.push(random)
		}
	}
}

function areEqual(post1: Post, post2: Post): boolean {
	return post1.data.title === post2.data.title && post1.body === post2.body && post1.id === post2.id
}
