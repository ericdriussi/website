import { uiConfig } from "../../consts"
import type { Post } from "../../content/config"

export class RelatedPostsMerger {
	public static readonly minAmount = 2

	static merge(
		amount: number,
		next: Post[],
		prev: Post[],
		bySeries: Post[],
		byCategory: Post[],
		byTags: Post[],
	): Post[] {
		if (amount < this.minAmount) {
			return []
		}
		const desiredAmountByDate = idealAmountByDate(amount)
		const nextAmount = prev.length === 0 ? desiredAmountByDate : desiredAmountByDate / 2
		const topNext = next.slice(0, nextAmount)
		const prevAmount = next.length === 0 ? desiredAmountByDate : desiredAmountByDate / 2
		const topPrev = prev.slice(0, prevAmount)

		const desiredAmountByTaxonomy = idealAmountByTaxonomy(amount)

		const missingBeforeSeries = desiredAmountByDate - lenOf([topNext, topPrev])
		const amountBySeries = missingBeforeSeries > 0 ? desiredAmountByTaxonomy * 2 : desiredAmountByTaxonomy
		const topBySeries = bySeries
			.filter((post) => !isPostPresentIn(post, [topNext, topPrev]))
			.slice(0, amountBySeries)

		const missingBeforeCategories =
			desiredAmountByDate + desiredAmountByTaxonomy - lenOf([topNext, topPrev, topBySeries])
		const amountByCategory = missingBeforeCategories > 0 ? desiredAmountByTaxonomy * 2 : desiredAmountByTaxonomy
		const topByCategory = byCategory
			.filter((post) => !isPostPresentIn(post, [topNext, topPrev, topBySeries]))
			.slice(0, amountByCategory)

		const topByTags = byTags
			.filter((post) => !isPostPresentIn(post, [topNext, topPrev, topBySeries, topByCategory]))
			.slice(0, amount - lenOf([topNext, topPrev, topBySeries, topByCategory]))

		return [...topNext, ...topPrev, ...topBySeries, ...topByCategory, ...topByTags].slice(0, amount)
	}
}

function idealAmountByDate(amount: number) {
	return Math.max(2, amountOrFactorOf(amount) * 2)
}

function idealAmountByTaxonomy(amount: number) {
	return Math.max(1, amountOrFactorOf(amount))
}

function amountOrFactorOf(amount: number) {
	return Math.floor(amount / uiConfig.relatedPostsFactor)
}

function isPostPresentIn(post: Post, arrays: Post[][]) {
	return arrays.some((array) => array.some((p: Post) => areEqual(p, post)))
}

function lenOf(arrays: Post[][]) {
	return arrays.reduce((acc, curr) => acc + curr.length, 0)
}

function areEqual(post1: Post, post2: Post): boolean {
	return post1.data.title === post2.data.title && post1.body === post2.body && post1.id === post2.id
}
