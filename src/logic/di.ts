import { PostService } from "./service/PostService"
import { PostRepo } from "./storage/PostRepo"
import { Prod } from "./storage/ProdCollectionWrapper"
import { TaxonomyRepo } from "./storage/TaxonomyRepo"

export function initService(): PostService {
	const postRepo = PostRepo.for(Prod.Collection())
	const taxonomyRepo = TaxonomyRepo.for(Prod.Collection())
	return new PostService(postRepo, taxonomyRepo)
}
