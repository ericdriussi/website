import { visit } from "unist-util-visit"

export function codeBlocksFormatter() {
	return () => {
		// @ts-ignore
		return (tree) => {
			visit(tree, "code", (node) => {
				const frame = getFrame(node)
				const showLineNumbers = getShowLineNumbers(node)
				node.meta = node.meta ? `${node.meta} ${frame} ${showLineNumbers}` : `${frame} ${showLineNumbers}`
			})
		}
	}
}

function getFrame(node: { lang: string }) {
	const lang = node.lang
	return lang ? `frame=terminal title="${lang}"` : "frame=none"
}

function getShowLineNumbers(node: { value: string }) {
	const lineCount = (node.value.match(/\n/g) || []).length + 1
	return lineCount < 10 ? "showLineNumbers=false" : "showLineNumbers=true"
}
