// @ts-nocheck
import { visit } from "unist-util-visit"

export function h1HeadingsRemover() {
	return () => {
		return (tree) => {
			if (hasH1(tree)) {
				visit(tree, "heading", (node) => {
					if (node.depth < 6) {
						node.depth += 1
					}
				})
			}
		}
	}
}

function hasH1(tree): boolean {
	return tree.children.some(isH1)
}

function isH1(node): boolean {
	return node.type === "heading" && node.depth === 1
}
