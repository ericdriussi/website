import { visit } from "unist-util-visit"

const contentImagePath = "../../assets/content/"

export function imagePathFormatter() {
	return () => {
		// @ts-ignore
		return (tree) => {
			visit(tree, "image", (node) => {
				if (isRelInCwd(node.url)) {
					node.url = `${contentImagePath}${rmRelIndicator(node.url)}`
				}
			})
		}
	}
}

function isRelInCwd(path: string) {
	return path.startsWith("./")
}

function rmRelIndicator(path: string) {
	return path.slice(2)
}
