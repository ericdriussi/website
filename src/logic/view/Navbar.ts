import { uiConfig } from "../../consts"
import { ScrollHandler } from "./ScrollHandler"

export function loadNavbarPeekabooEvent() {
	const header = document.querySelector("header")
	if (!header) return

	ScrollHandler.init()
		.onScrollDown(() => {
			header.style.top = `-${uiConfig.navbarHeight}`
		})
		.onScrollUp(() => {
			header.style.top = "0"
		})
		.registerEvents()
}
