export class ScrollHandler {
	private scrollThreshold = 1
	private upCallbacks: (() => void)[] = []
	private downCallbacks: (() => void)[] = []

	static init(): ScrollHandler {
		return new ScrollHandler()
	}

	onScrollUp(callback: () => void): ScrollHandler {
		this.upCallbacks.push(callback)
		return this
	}

	onScrollDown(callback: () => void): ScrollHandler {
		this.downCallbacks.push(callback)
		return this
	}

	registerEvents(): void {
		let lastPos = window.scrollY

		window.addEventListener("scroll", () => {
			requestAnimationFrame(() => {
				switch (this.scrollDir(lastPos, this.scrollThreshold)) {
					case "down":
						this.downCallbacks.forEach((callback) => callback())
						break

					case "up":
						this.upCallbacks.forEach((callback) => callback())
						break
				}

				lastPos = window.scrollY
			})
		})
	}

	// This is needed for vimium compat
	private scrollDir(lastPos: number, scrollThreshold: number) {
		const scrollDiff = window.scrollY - lastPos
		const isScrollingDown = scrollDiff > scrollThreshold
		const isScrollingUp = scrollDiff <= -scrollThreshold
		if (isScrollingDown) {
			return "down"
		}
		if (isScrollingUp) {
			return "up"
		}
		return "none"
	}
}
