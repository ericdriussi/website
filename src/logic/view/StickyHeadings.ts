import { uiConfig } from "../../consts"
import { ScrollHandler } from "./ScrollHandler"

export function loadStickyHeadersOnScrollEvent() {
	const headings = Array.from(document.querySelectorAll("h2"))

	ScrollHandler.init()
		.onScrollDown(() => {
			const topZIndex = maxZIndex(headings)
			headings
				.filter(visible)
				.filter(belowOriginalPosition)
				.forEach((heading, i) => {
					stickify(heading, i)

					const isNotOnTop = getZIndex(heading) !== topZIndex
					if (isNotOnTop) {
						heading.style.top = `-${vertSizeOf(heading)}px`
					}
				})
		})

		.onScrollUp(() => {
			const topZIndex = maxZIndex(headings)
			headings.filter(visible).forEach((heading) => {
				const isOnTop = getZIndex(heading) === topZIndex
				heading.style.top = isOnTop
					? uiConfig.navbarHeight
					: `calc(${uiConfig.navbarHeight} - ${vertSizeOf(heading)}px)`

				if (isAboveOriginalPosition(heading)) {
					unStickify(heading)
				}
			})
		})
		.registerEvents()
}

function visible(heading: HTMLHeadingElement) {
	const viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight)
	const rect = heading.getBoundingClientRect()
	return !(rect.bottom < 0 || rect.top - viewHeight >= 0)
}

function stickify(heading: HTMLHeadingElement, index: number) {
	heading.style.position = "sticky"
	heading.style.background = "var(--bg)"
	heading.style.zIndex = "5" + index
	heading.style.transition = "top 0.1s"
	heading.style.top = "0"
	heading.style.boxShadow = "0px 10px 10px 10px rgba(40, 40, 40, 1)"
}

function unStickify(heading: HTMLHeadingElement) {
	heading.style.position = "static"
	heading.style.background = "none"
	heading.style.zIndex = "auto"
	heading.style.transition = "none"
	heading.style.borderBottom = "none"
	heading.style.boxShadow = "none"
}

function isAboveOriginalPosition(heading: HTMLHeadingElement) {
	return !belowOriginalPosition(heading)
}

function belowOriginalPosition(heading: HTMLHeadingElement) {
	return window.scrollY >= heading.offsetTop - heading.offsetHeight
}

function vertSizeOf(heading: HTMLHeadingElement) {
	return heading.getBoundingClientRect().height
}

function maxZIndex(headings: HTMLHeadingElement[]) {
	return Math.max(...headings.map(getZIndex))
}

function getZIndex(heading: HTMLHeadingElement) {
	const zIndex = parseInt(heading.style.zIndex)
	return isNaN(zIndex) ? 0 : zIndex
}
