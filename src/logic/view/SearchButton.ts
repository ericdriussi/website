export class SearchButton extends HTMLElement {
	connectedCallback() {
		const searchBarId = this.dataset.searchbarid || ""
		const searchBar = document.getElementById(searchBarId)

		const searchButton = document.getElementById("search-button")
		searchButton?.addEventListener("click", () => {
			togglePagefind()
		})

		document.addEventListener("keydown", (e) => {
			if (e.key === "Escape") {
				hideSearchBar()
			}
		})

		function togglePagefind() {
			if (searchBar?.style.display === "block") {
				hideSearchBar()
			} else {
				focusSearchBar()
			}
		}

		function hideSearchBar() {
			searchBar?.style.setProperty("display", "none")
		}

		function focusSearchBar() {
			searchBar?.style.setProperty("display", "block")
			searchBar?.querySelector("input")?.focus()
		}
	}
}
