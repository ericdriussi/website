const isElement = <T extends Element>(element: unknown, type: new () => T): element is T => element instanceof type

export function loadDropdownCloseOnClickEvent() {
	const form = document.querySelector("form")
	const toggle = document.getElementById("toggle")

	if (!isElement(form, HTMLFormElement) || !isElement(toggle, HTMLInputElement)) {
		return
	}

	document.addEventListener("mousedown", (event: MouseEvent) => {
		const target = event.target
		if (!(target instanceof Node)) {
			return
		}

		const clickedOutside = !form.contains(target)
		const menuIsOpened = toggle.checked
		if (clickedOutside && menuIsOpened) {
			toggle.checked = false
		}
	})
}
