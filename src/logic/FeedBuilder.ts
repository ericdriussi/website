import sanitizeHtml from "sanitize-html"
import MarkdownIt from "markdown-it"
import { site } from "../consts"
import type { Post } from "../content/config"

export function feedOf(posts: Post[]) {
	return {
		title: site.title,
		description: site.description,
		site: site.url,
		items: posts.map((post) => ({
			title: post.data.title,
			link: `/${post.collection}/${post.slug}/`,
			pubDate: post.data.published,
			description: post.data.description,
			content: sanitizeHtml(new MarkdownIt().render(post.body), {
				allowedTags: sanitizeHtml.defaults.allowedTags.concat(["img"]),
			}),
		})),
	}
}
