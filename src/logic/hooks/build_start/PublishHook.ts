import fs from "fs"
import path from "path"
import matter from "gray-matter"
import { Publisher } from "./Publisher"

export function publishOldestReadyDraft() {
	return {
		"astro:build:start": () => {
			if (!process.env.AUTOPUB) return

			const publisher = new Publisher({
				fs,
				path,
				matter: { parse: matter, stringify: matter.stringify },
				baseDir: process.cwd(),
			})
			publisher.publishDrafts()
		},
	}
}
