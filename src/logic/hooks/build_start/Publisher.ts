import { execSync } from "child_process"
import { readFileSync, readdirSync, statSync, writeFileSync, renameSync } from "fs"
import { join } from "path"

// Wrap dependencies for testing
interface FS {
	readdirSync: typeof readdirSync
	readFileSync: typeof readFileSync
	statSync: typeof statSync
	writeFileSync: typeof writeFileSync
	renameSync: typeof renameSync
}
interface Path {
	join: typeof join
}
interface Matter {
	// eslint-disable-next-line
	parse: (fileContents: string) => { data: Record<string, any>; content: string }
	// eslint-disable-next-line
	stringify: (content: string, data: Record<string, any>) => string
}

export class Publisher {
	private fs: FS
	private path: Path
	private matter: Matter
	private postsDirectory: string
	private draftsDirectory: string

	constructor(params: { fs: FS; path: Path; matter: Matter; baseDir: string }) {
		this.fs = params.fs
		this.path = params.path
		this.matter = params.matter
		this.postsDirectory = this.path.join(params.baseDir, "src/content/blog")
		this.draftsDirectory = this.path.join(params.baseDir, "src/content/draft")
	}

	public publishDrafts(): void {
		const drafts = this.fs.readdirSync(this.draftsDirectory)
		const sortedDrafts = drafts.sort((a, b) => {
			const filePathA = this.path.join(this.draftsDirectory, a)
			const filePathB = this.path.join(this.draftsDirectory, b)

			// Compare last commit time instead of mtime, since a clone would reset all mtimes
			const lastCommitDateA = execSync(`git log -1 --format="%at" -- "${filePathA}"`).toString().trim()
			const lastCommitDateB = execSync(`git log -1 --format="%at" -- "${filePathB}"`).toString().trim()
			return lastCommitDateA.localeCompare(lastCommitDateB)
		})

		for (const draft of sortedDrafts) {
			const filePath = this.path.join(this.draftsDirectory, draft)
			const fileContents = this.fs.readFileSync(filePath, "utf-8")
			const { data, content } = this.matter.parse(fileContents)

			if (!data.is_ready) continue

			data.published = new Date().toLocaleDateString("en-GB", { day: "2-digit", month: "short", year: "numeric" })
			delete data.is_ready

			console.info(`Publishing draft: ${data.title}`)

			const updatedContent = this.matter.stringify(content, data)
			this.fs.writeFileSync(filePath, updatedContent)

			const newFilePath = this.path.join(this.postsDirectory, draft)
			this.fs.renameSync(filePath, newFilePath)
			break
		}
	}
}
