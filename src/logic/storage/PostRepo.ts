import type { Post } from "../../content/config"
import type { CollectionWrapper } from "./CollectionWrapper"

export class PostRepo {
	private collection: CollectionWrapper

	private constructor(collection: CollectionWrapper) {
		this.collection = collection
	}

	static for(collection: CollectionWrapper) {
		return new PostRepo(collection)
	}

	async all(): Promise<Post[]> {
		if (process.env.DEV) {
			return (await this.collection.get("draft")).concat(await this.collection.get("blog"))
		}
		return await this.collection.get("blog")
	}

	async allAfter(date: Date): Promise<Post[]> {
		return (await this.collection.get("blog")).filter((p) => p.data.published > date)
	}

	async allBefore(date: Date): Promise<Post[]> {
		return (await this.collection.get("blog")).filter((p) => p.data.published < date)
	}

	async random(): Promise<Post> {
		const posts = await this.collection.get("blog")
		return posts[Math.floor(Math.random() * posts.length)]
	}
}
