import type { Post } from "../../content/config"
import type { CollectionWrapper } from "./CollectionWrapper"

export type TaxonomyWithCount = { taxonomy: string; count: number }

export class TaxonomyRepo {
	private collection: CollectionWrapper

	private constructor(collection: CollectionWrapper) {
		this.collection = collection
	}

	static for(collection: CollectionWrapper) {
		return new TaxonomyRepo(collection)
	}

	async sortedSeries(): Promise<string[]> {
		const allPosts = await this.collection.get("blog")
		return Array.from(new Set(allPosts.map((post) => post.data.series)))
			.filter((series): series is string => !!series)
			.sort()
	}

	async categoriesWithCount(): Promise<TaxonomyWithCount[]> {
		const allPosts = await this.collection.get("blog")
		const categories = Array.from(allPosts.map((post) => post.data.category))
			.filter((category): category is string => !!category)
			.map((category) => category.toLowerCase())

		return this.countAndSort(categories)
	}
	async tagsWithCount(): Promise<TaxonomyWithCount[]> {
		const allPosts = await this.collection.get("blog")
		const tags = Array.from(allPosts.flatMap((post) => post.data.tags || []))
			.filter((tag): tag is string => !!tag)
			.map((tag) => tag.toLowerCase())

		return this.countAndSort(tags)
	}

	private countAndSort(words: string[]): TaxonomyWithCount[] {
		const wordsWithCounts = this.count(words)

		return Object.entries(wordsWithCounts)
			.map(([word, count]) => ({ taxonomy: word.toLowerCase(), count }))
			.sort((a, b) => b.count - a.count)
	}

	private count(words: string[]): Record<string, number> {
		return words.reduce(
			(counts, word) => {
				counts[word] = (counts[word] || 0) + 1
				return counts
			},
			{} as Record<string, number>,
		)
	}

	async allInSeries(series: string | undefined): Promise<Post[]> {
		if (!series) return []

		const allPosts = await this.collection.get("blog")
		return allPosts.filter((post) => post.data.series).filter((post) => post.data.series === series)
	}

	async allWithCategory(category: string | undefined): Promise<Post[]> {
		if (!category) return []

		const allPosts = await this.collection.get("blog")
		return allPosts.filter((post) => post.data.category).filter((post) => post.data.category === category)
	}

	async allWithSomeOf(tags: string[] | undefined): Promise<Post[]> {
		if (!tags) return []

		const allPosts = await this.collection.get("blog")
		return allPosts
			.filter((post) => post.data.tags)
			.filter((post) => tags.filter((tag) => tag).some((tag) => post.data.tags?.includes(tag)))
	}
}
