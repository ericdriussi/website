import { getCollection } from "astro:content"
import type { CollectionWrapper } from "./CollectionWrapper"

export class Prod implements CollectionWrapper {
	get = getCollection

	public static Collection() {
		return new Prod()
	}
}
