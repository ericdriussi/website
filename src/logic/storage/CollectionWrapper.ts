import type { Post } from "../../content/config"

export interface CollectionWrapper {
	get: (collection: "blog" | "draft") => Promise<Post[]>
}
