export class Slugifier {
	public static slugify(text: string): string {
		return text.trim().toLowerCase().replace(/\s+/g, "-").replace(/^-|-$/g, "")
	}
}
