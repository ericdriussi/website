export class ActivePath {
	static check(href: string, pathname: string): boolean {
		const hrefSegments = href.split("/").filter(isNotEmpty)
		const pathnameSegments = pathname.split("/").filter(isNotEmpty)

		if (doesNotApply(hrefSegments, pathnameSegments)) {
			return false
		}

		if (allAreEqual(hrefSegments, pathnameSegments)) {
			return true
		}

		if (isRoot(hrefSegments) && !isPaginating(pathnameSegments)) {
			return allAreEqual(hrefSegments, pathnameSegments)
		}

		return allButLastAreEqual(hrefSegments, pathnameSegments)
	}
}

function isNotEmpty(segment: string): boolean {
	return segment.trim().length > 0
}

function doesNotApply(hrefSegments: string[], pathnameSegments: string[]) {
	const noSegments = hrefSegments.length < 1 || pathnameSegments.length < 1
	const hrefIsBigger = hrefSegments.length > pathnameSegments.length
	return noSegments || hrefIsBigger
}

function allAreEqual(hrefSegments: string[], pathnameSegments: string[]) {
	return oneContainsTheOther(hrefSegments, pathnameSegments) && oneContainsTheOther(pathnameSegments, hrefSegments)
}

function isPaginating(pathnameSegments: string[]) {
	const lastPathnameSegment = pathnameSegments[pathnameSegments.length - 1]
	return isNumber(lastPathnameSegment)
}

function isNumber(lastPathnameSegment: string) {
	return !isNaN(Number(lastPathnameSegment))
}

function allButLastAreEqual(hrefSegments: string[], pathnameSegments: string[]) {
	return oneContainsTheOther(hrefSegments, pathnameSegments) && hrefSegments.length === pathnameSegments.length - 1
}

function oneContainsTheOther(one: string[], theOther: string[]) {
	return one.every((segment, index) => segment === theOther[index])
}

function isRoot(hrefSegments: string[]) {
	return hrefSegments.length <= 1
}
