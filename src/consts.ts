export const site = {
	title: "dev.in.the.shell",
	description: "Yet another dev blog",
	url: "https://devintheshell.com",
	favicon: "/favicon.svg",
	lang: "en",
}

export const uiConfig = {
	navbarHeight: "3rem",
	pageSize: 10,
	relatedPostsFactor: 5,
	searchBarId: "search-bar",
}

export const dev = {
	e2eDebugPort: 9222,
	serverPort: 4321,
}
