import rss from "@astrojs/rss"
import { feedOf } from "@logic/FeedBuilder"
import { initService } from "@logic/di"

export async function GET() {
	const service = initService()
	const posts = await service.allSorted()
	return rss(feedOf(posts))
}
