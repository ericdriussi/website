# About me

Hi there!

I write about software, open source, stuff I see at work, things I learn or found useful, frustrating or interesting.

I used to be a Physiotherapist before Covid hit, now I spend my days hitting my keyboard pretending I know what I'm doing.

You can see what I'm up to on [GitHub](https://github.com/EricDriussi) (although I keep my personal stuff is on [GitLab](https://gitlab.com/ericdriussi)) and get in touch through [email](mailto:ericdriussi@gmail.com).

I might take a while to respond, but I will.
