import { defineCollection, z, type CollectionEntry } from "astro:content"

const blog = defineCollection({
	schema: ({ image }) =>
		z.object({
			title: z.string(),
			description: z.string(),
			published: z.coerce.date(),
			updated: z.coerce.date().optional(),
			image: image(),
			tags: z.array(z.string().min(1, "Cannot add an empty tag")).optional(),
			category: z.string().min(1, "Cannot add an empty category").optional(),
			series: z.string().min(1, "Cannot add an empty series").optional(),
			pinned: z.boolean().default(false).optional(),
		}),
})

const draft = defineCollection({
	schema: ({ image }) =>
		z.object({
			title: z.string(),
			description: z.string().optional(),
			image: image().optional(),
			tags: z.array(z.string()).optional(),
			category: z.string().optional(),
			series: z.string().optional(),
			is_ready: z.boolean().optional().default(false),
		}),
})

export const collections = { blog, draft, page: {} }
export type Post = CollectionEntry<"blog">
