---
title: Are IDEs worth it?
description: An overview on how they compare to code editors
image: ../../assets/content/editor.jpg
category: tools
tags:
  - IDE
  - editor
is_ready: true
---

Much of the eternal online debate about IDEs and code editors is either out of date, out of context or out of both.

Let's go over some of these misconceptions and some less talked-about points, and evaluate which alternative makes more or less sense and in what context.

## Speed

IDEs are slow, or so the trope goes.

While this is most noticeable at startup, this slowness is not necessarily limited to startup times.
It makes sense: IDEs are constantly trying to help and make suggestions. This has a compute cost.

Of course, given good enough hardware or simple enough projects, this might not really be an issue.
Still, that doesn't change the fact that, _ceteris paribus_, a code editor is faster.

Some might say that it doesn't matter, that it's not noticeable or that it's more than worth it.
Those are some _"that's like your opinion dude"_ kinda points more than anything else.

Whether you notice it or not is a matter of how sensitive **you are** to things like input lag or what you consider fast enough.
Whether it's worth it or not depends on a bunch of different things, like the stack **you are working with**, the particular IDE you are using or how close you can get by adding plugins to a code editor.

In other words: it's context dependent and up to you if this is an issue or not.

## LSP

Traditionally, the only way to get decent code completion and navigation was using an IDE.
In that sense, the developer experience used to be much, much better for IDEs than for code editors.

Since [LSP](https://microsoft.github.io/language-server-protocol/) came out however, this is no longer the case.
Long story short, the Language Server Protocol standardizes how an editor or IDE interacts with a language, which enables any LSP-capable editor to perform much like an IDE.

Newer languages have official LSPs available on day one and even older ones often have unofficial alternatives made by the community.
To be clear, not all languages benefit from this. Obvious examples are Java and C#: good luck using them without Visual Studio or IntelliJ.

This used to be a big difference between using an IDE or not, but nowadays, its more a matter of what _else_ can an IDE provide.

## Distractions

Some of us **need** a distraction-free environment.

First thing I do after installing an app or subscribing to a service is disabling all non-critical notifications.
I don't want to deal with useless popups, sounds or flashing icons.

Clearly, IDEs are not made for people that have trouble focusing.

An IDE is always trying to help the user, which means bringing stuff to your attention. Constantly.
This is great if you need the help and can handle the distraction, but absolutely unbearable if you don't.

You can of course tone all of this down, just keep in mind that you are swimming against the tide: the IDE doesn't really want to shut up.

## Learning

As the name suggests, IDEs integrate _everything you need_<sup><sup>TM</sup></sup> under a single interface.

It's super convenient, but has the unexpected side effect of allowing you to treat all of those "_things you need_" as a black box.

You abstract away complexity, but lose the understanding of the underlying systems.

I've personally met and worked with people that only know how to interact with a database through their IDEs UI. Not **an** IDE, **their** IDE.
The same goes for docker, running tests and, worse of all, **running the code itself**.

Let me repeat, some professional developers can only run the code they write through the green "Play" button of their IDE. Talk about vendor lock-in.

Now, this is a human issue, not an IDE issue. People that behave like that tend to have the same exact problem with all the software they use.
Still, humans gravitate towards comfort and more often than not the (apparently) easier path will be chosen. Even if that means settling for ignorance.

People that have only ever driven automatic are unlikely to learn how to drive a manual out of curiosity, even though they might benefit from the knowledge at some point.

This might be a worthwhile trade-off, or it might not. But it is **a trade-off**, and we should be aware of it.

## Death by feature

Code editors are usually pretty bare-bones out of the box: the user is supposed to **add features as needed**.
This usually results in a (sometimes unreasonably long) list of plugins.
The idea here is to end up with the features needed by the user and nothing else. Keep things relatively simple.

Conversely, IDEs come with **all the features**: the ones you need and the ones you don't.
This of course is far removed from the good old _"do one thing and do it well"_ UNIX principle, and may introduce unexpected complexity or interactions under the hood.
The point is for the user to find everything he might need right from the get-go.

If you use a code editor expecting to not have to configure anything up front, you are going to have a bad time.
If you use an IDE expecting it not to pollute your dev environment with opinionated solutions, you are going to have a bad time.

## IDEs actual use case

This all seems pretty negative towards IDEs, but there are a couple of very key areas where they shine so much more than code editors it's not even fair.

### Refactoring

This is not necessarily true for every IDE out there, but once you learn to refactor code using any of JetBrains IDEs, there is absolutely no turning back.

The amount of manual labor it eliminates, of refactors you wouldn't even dare to do by hand, the time it saves: there's simply no comparison with any code editor.
Yes, there are plugins that somehow try to mimic these features. But it's not even the same league.

There's just no way around it: If you do frequent heavy refactoring, right now there is nothing that even comes close.

### Intelligent analysis and suggestions

The counterpart of all that annoying slowness: most of the time, it feels like the IDE is reading your mind.
It just knows much more about the project than what is currently possible through the LSP.

Yes you can get good suggestions in any LSP-capable code editor, but a decent IDE will suggest stuff like variable names so cleverly you'd think your computer is actually doing your job.
The suggestions regarding code structure, repetition, standards and general best practices are just not there for code editors. Yes, you can use linters, but it's just another level of awareness.

Mind you, this was the case even before the AI craze.

### IDEs as a crutch

The less you know about a stack or project, the more useful an IDE is.
Which is not to say that senior devs shouldn't use them or they are "_for noobs_", far from it.

This is precisely why we love them so much in consulting: we often get thrown head first into new projects with god knows what weird or ancient stack, using languages we are not super familiar with, while expecting us to refactor, stabilize or improve the codebase.
Using an IDE allows me to focus on _the system_ first, and leave the stack/language details for later. It fills any gaps in my knowledge and allows me to start being helpful faster.

This might hurt some people's ego or pride. You should leave those out of your workplace.

## Use both

For whatever reason, this issue is often presented as a false dichotomy (as with most other online debates): either use a fully featured IDE for everything or a dementedly minimalist setup with barely any software apart from a terminal emulator.

This is a bit silly, there's no reason why you shouldn't use both. Choose the best tool for the job, don't fall in love with software.

You don't need an IDE to edit a `bash` script or a `yaml` file.
You don't want to work on a legacy `java` codebase with a text editor.
