---
title: Software licences
published: 2023-02-10T16:15:44Z
image: ../../assets/content/copyleft.webp
category: write-ups
tags:
    - copyright
    - copyleft
    - licence
    - open-source
description: A quick run down
updated: Dec 15 2024
---

An overview of different open source software license types with a quick run down of the most common ones.

### TL;DR

- Code with no license attached is under exclusive copyright of the creator.
  - This applies for public GitHub/GitLab repos: users can only see and fork them.
- GPL requires all derivative work to be licensed with the same license.
- MIT has basically no restrictions and is the most widely used.
- Apache is like MIT, **but** patent rights are explicitly granted to the users.
- MPL is like MIT, **but** modifications must share the same license.
- BSD-3 is like MIT, **but** the project name cannot be used as endorsement.

You can read more about the various licenses [here](https://choosealicense.com/licenses/) and [here](https://tldrlegal.com/).

## Permissive vs Restrictive

These words get thrown around a lot, but their meaning might seem counter-intuitive.

In a nutshell, restrictive licenses work a bit like viruses: They require derived work to share the same license. They **restrict** derivative work to that specific license.
In contrast, permissive licenses have a more _"do what you want"_ approach.

So in this context, these concepts are not from the point of view of the user (none of these license restricts the user in any way) but from the perspective of other developers/business, which may or may not do what they want with the software.

## MIT & similar licenses

**MIT** licensed code can be used by whoever to do whatever.
The only restriction is that the original "owner" cannot be held liable.

A business that stumbles across some MIT licensed code can feel free to use it however it wants.

Clearly, this license is the most business-friendly, in the sense that businesses are not restricted in any way by it.
The story is however quite different from the developers point of view, as this license is perfectly fine with _BigTechCorp_ using your code for profit and giving nothing back.

It's by far the most widely used, not least due to its [simplicity](https://tldrlegal.com/license/mit-license#fulltext).

Code licensed under the **Apache** license is functionally in the same situation, with the added technicality that patent rights are explicitly granted to the users.
This is done to prevent patent-holding contributors from possibly suing users of patented code.

The **BSD-3** license is a lot like MIT, with the added restriction that the original project name cannot be used as endorsement of derivative work.
So if a business decides to redistribute a piece of BSD-3 licensed software, it could not use the name of that software to promote or endorse their product in any way.

**MPL** is technically a restrictive license, but it only is so for **modifications** of licensed code. This is to say, all modifications of an MPL licensed code must also be licensed under an MPL license.

## GPL & friends

**GPL** is a family of restrictive, copyleft licenses that broadly require all derivative work to be distributed under the same license.
Code under these licenses is generally considered not only _[Open Source](https://opensource.org/osd)_, but _[Free and Open Source](https://www.gnu.org/philosophy/free-sw.en.html)_.

Code under these licenses can be used freely only in projects under the same or similarly restrictive licenses.
So if a business wants to use a piece of software under a GPL license, it can do so with the condition all code _derived_ from it is distributed under the same license.

This is why it is traditionally considered less business-friendly than the previously mentioned licenses: I can't just "steal" your work and profit, I have to "give away" the code as well.
Making money from GPL code requires the business to actually provide a service, add value, instead of merely hiding the code behind a paywall.

Note that internally or privately used codebases don't apply here: These licenses are concerned with code that is **distributed** (freely or not) to end users.
Also, the notion of **derivative work** here generally refers to any code that depends on and is distributed with the original work.
This however may vary from one type of GPL license to another.

### Lesser GPL and Affero GPL

A more permissive and restrictive version of the GPL respectively.

LGPL is non-restrictive as long as the licensed work is being used _"through interfaces provided by the licensed work"_.
AGPL is more restrictive in that when the licensed work is being used _"to provide a service over a network, the complete source code [...] must be made available"_.

Simply put:
**LGPL** draws an exception for libraries.
**AGPL** also considers it _"derived work"_ if it sits behind a network.

### GPLv2 vs GPLv3

Similarly, while GPLv2 includes no particular consideration regarding hardware, GPLv3 does:

> If the software is part of a consumer device, you must include the installation information necessary to modify and reinstall the software.

So in this case, distributing software as "part" of hardware does not exclude it from the GPL restrictions.

Why does this matter?
Well some companies thought that only software distributed by itself (think CDs or floppies in the olden days or a software download nowadays) would be subject to the license restrictions.
For the more curious, look up "Tivoization".

The new version of the license explicitly prevents this "misunderstanding".

## Other licenses

There are of course as many licenses as one is willing to look for.

The **Unlicense** for example [states](https://choosealicense.com/licenses/unlicense/) that the work is dedicated to the public domain.
No copyright, no restrictions, no strings attached.

Other honorable mentions are the [Do What The Fuck You Want To](https://tldrlegal.com/license/do-wtf-you-want-to-public-license-v2-(wtfpl-2.0)), the [I Don't Give A Fuck](https://tldrlegal.com/license/idgaf-v1.0), or the [Good Luck With That Shit](https://github.com/me-shaon/GLWTPL/blob/master/NSFW_LICENSE) licenses.
Some people just can't be bothered.

## No License?

So what happens if a piece of code is not licensed at all?
By default, the author has full exclusive copyright: nobody should do anything with that work without his/her permission.

This is irrelevant if the work is kept private, but might lead to interesting situations when sharing it.
Say a user stumbles upon an unlicensed piece of code on the internet.
This user has three options:

- Don't use the software.
- Negotiate a private license/bring a lawyer.
- _Yarr me salty seadog!_

This is awkward and can be messy to deal with, so please just license the work!

## GitHub TOS

This leaves us with a final consideration on GitHub [TOS](https://docs.github.com/en/site-policy/github-terms/github-terms-of-service#5-license-grant-to-other-users):

> By setting your repositories to be viewed publicly, you agree to allow others to view and fork your repositories.

So in trusting Microsoft, we agree that our **unlicensed, public** repos are free to view and fork, but nothing else.
This leaves us in the same situation described above so again: Just license your work!
