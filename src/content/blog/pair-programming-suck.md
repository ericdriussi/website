---
title: Pair programming doesn't have to suck
published: 2021-10-08T19:09:55+01:00
image: ../../assets/content/pair.webp
category: best-practices
tags:
    - pairing
    - pair programming
    - development
description: And you might want to do it more often
updated: Mar 06 2023
---

A while ago an interesting debate occurred between my coworkers regarding pair programming.
We all follow this practice at [LeanMind](https://leanmind.es) as much as possible, but some of us do so pretty much 24/7 while others have a more selective approach.
This led to an interesting conversation that made each other more aware of the way others view this practice.

![pair-love](./pair-love.webp)

## When is pairing not fit for purpose

It's only fair for me to point out some valid considerations regarding what can go wrong with pairing, before arguing why you should still do it.

### Productivity

One might argue that, if the task is simple enough, having two developers tackling it is a bit wasteful.
Sure, complicated o critical problems might require group collaboration but maybe if we could get more done on our own, we should.

This will probably resonate more with business people than with developers, we'll see why in a bit.

### It aint' easy

Some people are more social than others. Even the same person can be more social one day than the next, and that's okay.
There's an argument to be made for not feeling **pressured** to pair. It shouldn't be a mandatory thing, but rather a scenario to strive for.

Take a break from time to time, change partner, ease yourself into it.
Computer people are not really known for their social skills, nobody expects an introvert to want to spend 8 hours straight with a co-worker.

It might be healthier to think of pair programming as an objective in some cases, rather than '*a thing you just do*'.

### Everybody is different

Just like with any other human interaction or relation, some work and some don't.
This can be harsh and hard to deal with (for some more so than for others).

Some get distracted while being the '*navigator*' and wander around not really helping a lot.
Some want things done their way and can't seem to listen while being the '*driver*'.
Some may not be willing to swap out the role at all.

Dealing with these situations is quite simply **not developing software**.
And you can bet that your average developer has zero interest in resolving issues like these (although one could argue that they probably should).

### Juniors might hamper Seniors

No matter how you define '*Juniors*' and '*Seniors*' (or if you just don't care much for the terms at all), people with less knowledge and/or experience in any particular subject will inevitably take longer to understand things and have **a lot** of questions.

It's easy to see yourself pairing with someone more or less at the same level you are, but it can be annoying to have someone stopping you at every step to ask you basic (although very **valid**) questions.

One could consider pairing partners based on experience, although there are very valid reasons to mix and match experience levels.

![pair-strong](./pair-strong.webp)

## Pair as much as you can

Now for the good stuff.

---

### Beware the bias

I've been very lucky in this regard: I've written some code on my own, but I have never **worked** alone.

I started a while back in a team that felt more like a bunch of friends than a work group. Not that we weren't getting things done (we were even helping other teams on a regular basis), we just worked in a very friendly and enjoyable environment.

We laughed a lot and were there for one another, it really felt like hanging out with some friends.

Just keep in mind that someone less fortunate might not feel the same as I do about pairing, which is totally fair!

---

### Don't care much about productivity

It might be productive **in the short term** to have two developers doing two different tasks, but is it also in the long run?

When both codes need to be merged, but they seem like they come from two different worlds, is pairing still less productive?
What about when one of them spends two days looking for a bug that could have been easily found in half an hour by just having double the eyes looking at the code?
Don't you think you would benefit from a helping hand when you hit a dead end?
Wouldn't you **always** prefer to know **early** if there is a better way?
Do you really think that having some help or a different opinion makes you less productive?

Sure you could just ask for help when needed but that assumes everybody is mature enough to **explicitly and openly** admit they need help.

This is why I mentioned that business people might focus more on this than developers:
The usually just don't see or understand the present and long term differences between clean code and spaghetti code.
And that's not a criticism, it just isn't really their job to tell one apart from the other. They shouldn't be expected to.
If anything it's on developers for not knowing how to present these issues properly to the business.

### Majority of us are noobs (and that's fine)

Uncle Bob has repeated this fact time and time again: Developers more or less double every five years or so.
This means there are always **a lot** more inexperienced developers than experienced ones.
There is an obvious need to take Juniors and their learning process into account when thinking about anything related to software development.

It's kind of strange that '*shadowing*' isn't just standard practice in the industry, but if it were, it would benefit **greatly** from pairing.
I can't think of a faster way for someone to get the hang of a particular language, framework, methodology or project than for him to pair with an experienced developer.

Sure you can do test projects on your own (and it actually helps a lot), but the level of insight and '*know how*' you get from watching a Senior hands on with the code, asking questions and receiving corrections and guidance in real time is just on a completely different league.

Of course, that will hamper his speed (quite a lot at the beginning), especially if the more experienced one actually cares about his pairing partner getting better on the job at hand.
To this I would say...

### Developer 'flow' is no good

It sure feels good: Being on you own, doing your thing at light speed. Just ripping through the code and solving tickets left and right.
You look at your code after a week and have no clue what you wrote, why you wrote it that way or if there was a better way to do it. You overengineered half of it and didn't stop for a moment to see what the rest of the codebase looks like.

![success](./borat-success.webp)

I would argue that being in this state of '*Flow*' is only useful if you work on your own in a small and/or unimportant project.
It seems much, much better in the long run (and for the rest of the team) to have someone stop and question you every once in a while.
This will make you consider why you are doing what you are doing (and if you are making it unintelligible for the rest of us).
If you can explain it simply you can be sure that you know what you are doing (and you will have taught something new to your peer, making both of you better off).

Nobody wants to be distracted while solving a problem, but when doing things out of inertia we can all benefit from an external voice of reason.

### Good for the mind

Especially with remote work becoming the norm, it's not really healthy to spend 8 hours a day solving problems under stress on your own.
I know, it sounds strange, but we are supposed to be around each other. We are not built to be alone.

You might think you are better off on you own, but I can guarantee you will feel better with good company.
Pairing can be fun, relaxing and morally helpful.
Don't isolate yourself for no reason.

### Enforces soft skills

I've been pleasantly surprised with how important soft skills are in software development and I can see a couple of clear benefits of pair programming in this regard:

#### Leave your ego at the door

If done well, pairing will force you to listen to others, overcome your ego and accept what's best for the team.
You can never have too much of that.

#### Leave your personal problems at the door

Not only will this help you compartmentalize trouble (which is always good), you will **have** to *decouple* your mind from your personal issues.
That's just free counseling right there.

#### Forces professional behavior

A doctor doesn't just behave badly or work alone when he is '*not feeling it*'. He picks himself up and gives his best.
Why should we not do the same?

#### Soft skills Hard truth

The reality of soft skills is that there is no method to it. You can watch what you say and how you say it. You can be more assertive and sensitive.
But at the end of the day, you learn soft skills by throwing yourself into the fire.

You can't '*think your way*' through human interactions.
You deal with them as they come, in the context you struggle the most, screw up as many times as you need in order to get a feel for it and acquire that new skill (just like you would when learning to ride a bike).
Working alone won't help much with this.

When in doubt, program in pairs. It's good for everybody
