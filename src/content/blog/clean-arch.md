---
title: Clean Architecture
published: 2021-11-23T17:20:20Z
image: ../../assets/content/clean-main.webp
series: architecture
category: best-practices
tags:
    - architecture
    - design
    - clean code
    - clean architecture
description: Integrating previous designs
updated: Nov 24 2024
---

This is part of a series, [start here!](../arch-for-noobs)
<br>
<br>

This is Uncle Bob's attempt at synthesize previous architectural patterns and concepts.

Based on a common thread between [Ports & Adapters](../ports-and-adapters), [Onion](../onion-arch), [EBI](../ebi-arch), he wrote an [article](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) as _'an attempt at integrating all these architectures into a single actionable idea'_.[^1]

[^1]: From the [original article](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)

## High level

The original article starts with a diagram similar to this:

![diagram](./clean-arch-mine.webp)

### Project structure

At face value, the diagram seems to suggest a project structure as such:

```
Entities
├── Tenant
├── Landlord
├── Rent
Repositories
├── TenantRepo
├── LandlordRepo
├── RentRepo
UseCases
├── PayRent
├── RequestRent
├── RequestRepair
├── CalculateRent
...
```

There are however a couple of shortcomings here:

- **Low cohesion**: Modify one Use Case, and you'll have to change code in three different modules
- **No clear purpose**: A newcomer would have to dig through the directory structure to know what the application is for

Borrowing some key concepts like [Bounded Context from DDD](../ddd-strategy/#bounded-context), the previous system can be represented as such:

```
Tenant
├── Tenant
├── TenantRepo
├── PayRent
├── RequestRepair
Landlord
├── Landlord
├── LandlordRepo
├── RequestRent
Rent
├── Rent
├── RentRepo
├── CalculateRent
...
```

## Low level

There's a pretty useful diagram in the slides Bob uses in his conferences.

![bobs-flow](./clean-arch-flow-diagram.webp)

Cool, but a bit overwhelming. Let's start with Entity and Interactor[^2] and build the diagram with concepts from other notable architectures.[^3]

[^2]: From the [EBI architecture](../ebi-arch)
[^3]: [Start here](../arch-for-noobs)

![diagram-1](./clean-arch-diagram-1.png)

Think of the Interactor as the implementation of a use case of the application.

Thanks to [Ports & Adapters](../ports-and-adapters), we know that a use case should be defined as an abstraction to ensure inwards dependency.

![diagram-2](./clean-arch-diagram-2.png)

So if a use case is an abstract concept of _'what needs to be done'_, the Interactor is the concrete implementation of _'how exactly it will happen'_.

Entities usually require some sort of persistence, we can use a Driven Port/Adapter pair for that.

![diagram-3](./clean-arch-diagram-3.png)

Let's also represent the actor that will use the Driver Port from before, as well as the data structure (DTO) that will be shared between it and the Interactor.

![diagram-4](./clean-arch-diagram-4.png)

The controller could just as well be a CLI or a GUI.

Using a DTO here allows us to avoid exposing the Domain Entities outside the boundaries of the application. Speaking of boundaries.

![diagram-5](./clean-arch-diagram-5.png)

The red line marks the limit of our application logic and separates it from the pieces of the system that are necessarily coupled to the Infrastructure.

Now suppose that, when something happens in the system, we want to notify the user or update a UI element.

![diagram-6](./clean-arch-diagram-6.png)

Or using Uncle Bob's terminology:

![diagram-7](./clean-arch-diagram-7.png)

With this, we are back to his original diagram.

Let's review how the flow of execution would go for an incoming HTTP request:

1. The Request reaches the Controller
2. The Controller:
    1. Dismantles the Request and creates a Request Model with the relevant data
    2. Through the Boundary, triggers the Interactor
3. The Interactor:
    1. Finds the relevant Entities through the Entity Gateway
    2. Orchestrates interactions between entities
    3. Creates a Response Model with the relevant data and sends it to the Presenter through the Boundary

The diagram in the lower right corner of the first [image on the original post](https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg) might help visualize what's going on.

![flow](./flow.webp)

Swap _'Use Case Output/Input Port'_ for _'Boundary'_.
