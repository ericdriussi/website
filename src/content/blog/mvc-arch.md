---
title: MVC & MVVM
published: 2021-10-02T18:40:28+01:00
image: ../../assets/content/mvc-main.webp
series: architecture
category: best-practices
tags:
    - architecture
    - design
    - clean code
    - mvc
description: In the beginning was the Word, and the Word was "Separation of Concerns"
updated: Nov 18 2024
---

This is part of a series, [start here!](../arch-for-noobs)
<br>
<br>

After suffering the consequences of mashing everything together, someone got tired and [gave us MVC](https://folk.universitetetioslo.no/trygver/1979/mvc-2/1979-12-MVC.pdf).

## Model-View-Controller

In 1979, Trygve Reenskaug came up with this architecture as a way to solve some of the issues related with writing code for the machine and not for the human.

This was our first attempt at _'separation of concerns'_ and was guided by the following logic:

> Separate data, logic and presentation.

More accurately, it contemplated three basic units (or layers):

- **Model**: The business entities/logic.
- **View**: The UI.
- **Controller**: The _'procedural'_ or _'application'_ logic. It would guide all interactions between the previous two.

Would look something like this:

![MVC](./MVC.png)

If done right you would end up with multiple View-Controller pairs per screen, since each view should ideally only be responsible for a single piece of the UI (widget, button, text field...) and talk to a Controller if data was needed.

This also means that if multiple Views needed the same data, they would communicate with the same Controller.

Notice here that the Controller _reacts_ to the View, and manipulates the Model as a consequence.

The View would then react directly to the events triggered by the Model, updating the UI accordingly.

This forces a one directional flow in which the user interaction with the View determines what a specific Controller should do with the Models, which in turn updates the View directly.

This design is often still used in the front end (which is no surprise since it was created in the context of GUI reliant desktop applications).

All things considered, this approach leaves us with a couple of issues:

1. The View-Controller relation can get messy fast.
2. Having multiple Views per Controller can get even messier, since each View ideally corresponds to a piece of the UI.
3. The View is coupled directly to the Model.

### Model-View-ViewModel

That's what [John Gossman tried to solve](https://learn.microsoft.com/en-us/archive/blogs/johngossman/introduction-to-modelviewviewmodel-pattern-for-building-wpf-apps) around 2005.

Basically, he called the old Controller **ViewModel** and made it also responsible for the events fired by the Model.

So now, the flow of execution would stop in the ViewModel both on its way to the Model and on its way back to the View.

This new and improved 'Controller' now had the power to manipulate Models as well as to implement View specific logic (which made the View much simpler, so designers to focus on... Design).

Now View and ViewModel **must** have a 1:1 relation.

![MVVM](./MVVM.png)

As you might imagine, once the Model operations get complicated and/or there's a bunch of different data to manage or present together, it's easy to still wrangle things together.

The [EBI architecture](../ebi-arch) attempts to solve this.
