---
title: Blockchain 101
published: 2021-09-26T15:02:46+01:00
image: ../../assets/content/blockchain.webp
category: code
tags:
    - blockchain
description: Blockchain basics
updated: Jan 29 2023
---

## General structure

As the name implies, a blockchain is nothing more than a *chain* of *blocks*, where each block contains a collection of transactions and is connected (chained) to the previous block.
Kinda like a Git repo is made up of a bunch of commits linked to one another, each containing a bunch of changes.
Only in this case you could only commit new code to the repo, no rebasing or amending.

The chain can be (and usually is) distributed among multiple machines, thus it's decentralized nature.
This begs the question, what if I mess around with my copy of the chain?
Can I give myself a million coins just like that?

## Proof of work

To ensure the validity of the transactions (and that the blockchain has not been tampered with), all parties involved in the network need to agree on one 'correct' blockchain.

A common way to face this problem, although not the only one, is by using a **proof of work** system.

Simply put, the transaction finds its way into a block, which is then validated by solving an algorithm.

The lucky first will receive a portion of the transaction as a payment while the others will **verify** that the solution is correct (this is why you can't just create coins as you wish).

The algorithm needs to be quite expensive to compute for this to make sense, but we also want to verify and compare the results easily.
For this, we use hashes.

## Hashing

A Hash is a "*one way cryptographic function*".
Which means we can use it to encrypt data, but getting the original data from it's encrypted form is impossible.

This is useful because it makes it impossible to tamper with a computed block once it's hashed, while making it easy to compare it to others.

Plus, hashing has the added benefit of producing standard sized *blocks*.

## Transacting & Signatures

The last piece of the puzzle are transactions.

To send and/or receive coins, each user needs a *Wallet*.
This is little more than a key pair (a public key and a private key).

A transaction in this context is basically composed of the amount to send/receive, plus the sender and receiver public keys.

When a transaction is sent, it is singed with the sender's private key.
This means that anybody can confirm who the sender was by checking if the public key matches the signature of the transaction.

This way, we can trace the transactions as far back as we want, without exposing the private keys of the wallets involved.
