---
title: Docker 101
published: 2022-01-30T16:14:52Z
image: ../../assets/content/docker.webp
category: tools
tags:
    - docker
    - image
    - container
    - vm
description: Some very simple concepts
updated: Jan 29 2023
---

Basic docker-related vocabulary (namely Image, Container and Volume) with a brief explanation.
There are a lot of nuances I'll be glossing over.

We'll go into a bit more practical detail in the [follow-up post](../docker-102).

## Not a VM

It is usually described in detail how different Docker (or containerization more broadly) is a from a traditional Virtual Machine.

This is most definitely true: there are a number of low level and practical differences between these two technologies.
However, I would argue that pointing out those differences does little to help understand the vocabulary and concepts around Docker.

In fact, understanding VM-related concepts like _instance_ or _virtual hard disks_ and the difference between a VM definition/configuration and a specific _run_ of that VM can go a long way to help understand why docker can be so intricate and useful.

## Image

An **Image** is what defines the default composition and behavior of a **Container**.
Think of how the idea of a table is an abstract representation of a concrete, palpable table.

If we were talking about a VM, this would be more or less analogous to a VM's definition or configuration.

You define an **Image** in the corresponding [Dockerfile](../docker-102#dockerfile), and build it via the command line (or pull it from the web).

In a VM, configuration and behavior would usually go separately. Best you could do are snapshots.
An **Image** on the other hand, not only defines configuration (i.e. OS or installed software) but also behavior (i.e. commands to run, dependencies to install) of what we call **Containers**.

## Container

A **Container** is a concrete instantiation of what's defined by the corresponding **Image**.
Think of the palpable table from before.

This would be like a specific, concrete instance of your good old VM.

It's an isolated environment in which _'stuff'_ (might be your app, might be other things) happens.
You can think of a **Container** as the concrete instantiation of an **Image**
It gets created when you run said **Image**.

The technical differences between VMs and containerization are usually brought up at this point.
Just know that **Containers** are **stupidly efficient** compared to VMs, and a lot more versatile.

### Note

Technically, we say that **Images run in Containers**.

So **Containers** only hold that **Image** run.
This is because one **Image** can be executed multiple times in parallel, so you might have a bunch of **Containers** running with the same **Image** but possibly with different processes and/or outputs or results.

However, I reckon it's easier to visualize for a newcomer as explained above.

## Volume

Think of it as 'disk space' for a **Container** (or multiple **Containers**).
It's where docker will operate, its very own file system.

Plain and simple, it's the equivalent of a virtual hard disk for a VM.
It can be, and usually is, shared between multiple **Containers** and can easily communicate with (as in it's mounted to) the hosts file system.
