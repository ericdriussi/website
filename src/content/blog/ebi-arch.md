---
title: EBI Architecture
published: 2021-10-02T20:26:16+01:00
image: ../../assets/content/ebi-main.webp
series: architecture
category: best-practices
tags:
    - architecture
    - design
    - clean code
    - ebi
description: A step towards better design
updated: Nov 21 2024
---

This is part of a series, [start here!](../arch-for-noobs)
<br>
<br>

Created by [Ivar Jacobson in 1992](https://www.amazon.com/Object-Oriented-Software-Engineering-Driven-Approach/dp/0201403471) as EIC (Entity-Interface-Control) and renamed by Uncle Bob to Entity-Boundary-Interactor (EBI), It's a more back-end-focused version of the [MVC](../mvc-arch) architecture that came before.

![ebi](./ebi.webp)

## Entity

It consists **both** of the _'domain'_ entity and **all behavior strictly related to it**.

So in a very simple example, the _'Dog'_ Entity would hold the data regarding its breed, fur color, health, etc. as well as the logic required for it to behave as expected: a walk function, a bark function, etc.

Already back in '92, Jacobson was warning about _anemic entities_ and _god objects_.

## Boundary

The I/O interface of the system.

Think of it as the _'fence'_ of the domain (It's in the name).

All interaction between your code and the user on one side (GUI, CLI, etc.) and the infrastructure on the other (persistence, event queue, messaging, etc.) should be handled by this guy.

You might want to make it an interface and call it a [Port](../ports-and-adapters), but that's still 13 years away.

## Interactor

The ones in charge of validating I/O between Boundaries and Entities.

More important than this, they will be managing the **interactions** between Entities.
In practice, this means that all logic not belonging to or fitting in the Entities will end up here.

In our previous dog example, this role would be taken by the owner. Dogs don't play dead on their own, the owner (or trainer) needs to give the order.

Stretching the example a bit, dog to dog interaction is usually mediated by one or more humans (assuming they are pets).

The same applies here.

Of course, one interactor will often not be enough. You should end up with about one interactor per **use case**.

For every abstract operation a user could perform on your system, there should be an interactor ready to handle the use case.
