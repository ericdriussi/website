---
published: 2021-07-12T17:15:12+01:00
title: Operate and maintain your VPS
image: ../../assets/content/server.webp
category: self-host
tags:
    - vps
    - maintenance
    - server
description: Some basic tips and tricks
updated: Jan 29, 2023
---

## SSH

To ensure a secure SSH connection, it is best to not rely on password authentication.
Instead, we should use a key pair.

The idea is, we generate an ssh key for our machine and make our server trust it.

By doing this, we ensure that only the holder of the key can connect through SSH with the VPS, making access quicker, easily scriptable and brute-force proof.

### Generate the pair

```sh
ssh-keygen -t ed25519 -a 100 -C "your@mail.yes"
```

This command should prompt you for a path in which to store the keys (usually `~/.ssh/`) as well as a passphrase.

You may want to leave the passphrase blank if you plan on scripting on top of this connection.
Although more convenient, it is less secure.
In any case you can later run `ssh-keygen -p` to change or remove the passphrase.

You will now see a key pair in the path you selected. Let's get one of them on your server.

### Get the public key on your server

```sh
ssh-copy-id root@yourdomain.com
```

You'll have to enter your VPS's root user's password, after which your server will authorize access to the machine holding the SSH private key.

Log out and back in. If it bypasses the password prompt it worked!
If it didn't, check the permissions for both the keys and the `.ssh` directory.

### Disable password logins

Not a necessary step but if you want a reason to do it just check the output of `journalctl -xe` on your VPS.
To avoid brute force attacks, let's make sure logins are **only** allowed for the private keyholder.

Open `/etc/ssh/sshd_config` in your VPS and find/set the following lines as shown.

```
PasswordAuthentication no
PermitEmptyPasswords no
MaxAuthTries 3
```

This will harden your connection quite a bit, you can go the extra mile by setting up a non-root, sudo user and only connect to the VPS with that user.
If you go that route, make sure you set `PermitRootLogin no` to remove the possibility of a root login completely.

Of course, reload ssh: `systemctl reload sshd`

---

#### What if I lose the key?

**Don't. Backups are your friends.**

But in case you do, your VPS provider will likely offer some sort of local prompt emulated in a browser window.

Being local to the machine (or at least functioning as such), this will only be accessible to you after you log in to your VPS provider online account and won't prompt you for authentication
Then, simply set `PasswordAuthentication yes` in `/etc/ssh/sshd_config`.

---

## Rsync

You'll often find yourself needing to transfer files to and from your server.
Rsync is probably the easiest way to do it: it's fast, reliable and simple.

Make sure it's installed both on your local machine and on the server, then write (or make an alias for):

```sh
rsync -rtvzP /path/to/localFile user@yourSite.url:/path/on/the/server
```

This command will run <u>r</u>ecursively (including directories), transfer modification <u>t</u>imes (skips non modified files), <u>v</u>isualize the files being uploaded, compress (<u>z</u>?) files for upload and <u>P</u>ick up where it stopped in case of lost connection.

Of course, to download something from the VPS, just reverse the arguments.

## Cronjobs

There are certain routine tasks that are better left to Cron.
It will take care of running any command with a given frequency or repetition pattern.

Say, for example, that you want to automate updates for your server.
You could run `crontab -e` and insert something like `30 2 * * 0 apt -y update && apt -y upgrade` into the file (`:wq` to save and quit).

Let's break it down:

```
 .------------------ minute (0 - 59)
 | .--------------- hour (0 - 23)
 | | .------------ day of month (1 - 31)
 | | | .--------- month (1 - 12)
 | | | | .------ day of week (0 - 6)
 | | | | |
 * * * * *
30 2 * * 0 apt -y update && apt -y upgrade
```

Basically a machine friendly version of _'please update the system every Sunday at 2:30AM'_.

There's plenty you can do with cronjobs, [this](https://crontab.guru) website is a great tool to set specific patterns.

## Finding Things

You might use `whereis` or `which` to look for executables, but that can be insufficient depending on the task at hand.

`updatedb` can quickly index the whole system, which is neat since there's a tool called `locate` that can easily find files and directories whose paths contain any given string.

So if you run `updatedb` and then `locate cron`, you will see a list of files and directories containing `cron` in their paths.
Pretty cool!

## Ports

Often you'll want to know which ports are in use.
Assuming `ss` is installed, you could use something like:

```sh
sudo ss -tulpn | grep LISTEN
```

In some cases, you'll want to free a given port no matter who is occupying it.
Assuming `lsof` is installed:

```sh
lsof -i tcp:[PORT_TO_FREE] | awk 'NR!=1 {print $2}' | xargs kill
```

Keep in mind that this is a bit of a nuclear option, you might want to double-check what you are getting rid of!

## Troubleshooting

Troubleshooting issues is quite common when working on a VPS, and you'll likely find the root cause in the logs.

System-wide logs can be seen by running `journalctl -xe` (`-xe` is to make the results a bit more useful).
Of course, you can make your life easier if you know what you are after:

```sh
journalctl -xeu brokenApp
```

This will only show entries relevant to your `brokenApp`.

Of course, not all apps use the system logs. These will usually have their own under `/var/log/`.
For example, [NGINX](../nginx) has its access logs under `/var/log/nginx/access.log`, and the error logs in `/var/log/nginx/error.log`.
Have a look around, you'll definitely find what you're after.

Another useful troubleshooting utility is `systemctl`. It won't give you any logs, but you can use it to stop, restart, reload and start services manually and/or check their status:

```sh
systemctl status appNotWorking.service
```

This command will give you plenty of information regarding that specific service.
