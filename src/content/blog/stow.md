---
title: Tame your dotfiles with Stow
published: 2023-02-03T17:21:42Z
image: ../../assets/content/dotfiles.png
category: tools
tags:
    - stow
    - git
    - dotfiles
description: Stow your config files safely!
updated: Feb 16 2023
---

As soon as you start putting some time into your \*nix system configuration, you'll probably notice your config files getting out of hand.

Dotfiles all over the place, no good way to back them up, versioning is a pain, keeping your configs in sync in multiple OS or multiple computers becomes a chore.

You can use `stow` with `git` to tame them once and for all!

## TL;DR

1. Make a `~/dotfiles/` directory using this [repo](https://github.com/EricDriussi/dotfiles) as a reference for the dir structure.
1. **Move** your config files to the corresponding directory in `~/dotfiles/`.
1. Run `stow *` from `~/dotfiles/`.
1. Profit?

## Context

### Symlinks

Symbolic links are a way for us to make the OS believe there is a file where there's not.
It's a reference to a file that might be wherever we want.

You can create one from the command prompt with `ln -s <path_to_file> <path_to_link>`.

Naturally, the link will always show the same content the file has.

### Stow

This GNU command line utility is probably already installed on your \*nix system.
At a basic level, it's a symlink farm manager.
Give it a directory, and it will create a mirror directory with the same structure and links to the files in the original one.

> Stow is a symlink farm manager which takes distinct sets of software and/or data
> located in separate directories on the file system, and makes them all appear to be
> installed in a single directory tree.

This can be used as a really basic package manager, but we can make better use of it.

> However, Stow is still used not only for software package management, but also for
> other purposes, such as facilitating a more controlled approach to management of
> configuration files in the user’s home directory, especially when coupled with version
> control systems.

## The Plan

Say our favorite text editor expects a `vimrc` file in `.config/nvim/`.

If we run `stow` from a directory that mirrors the expected one, it will create the corresponding directory structure and symlink.

Let's see it in practice:

```
~
├──dotfiles [YOU_ARE_HERE]
│  ├── nvim
│  │  ├── .config
│  │  ├── nvim
│  │  │  ├── vimrc
...
```

Running `stow nvim` from `~/dotfiles/` will create the following structure, directories and all (assuming no conflicting files or directories where already there):

```
~
├── .config
├── nvim
│  ├── vimrc
...
```

This `vimrc` only references the one under `~/dotfiles/nvim/vimrc`.

### ⚠️ Careful

By default, `stow` will clone the dir structure **from** `cwd` (configurable through the `--dir` flag), **to** its parent directory (configurable through the `--target` flag).

This is why the examples assume that the dotfiles directory is in `~/dotfiles`.
I would advise you to follow this structure to avoid unpleasant surprises.

## The Execution

This means we can have all our config files in one neat repository like [this](https://github.com/EricDriussi/dotfiles) and deploy them with `stow *`.

The deploy command will only be needed once, since editing the files from the dotfiles directory will update the symlinks as well.

Whenever you update your config on one machine, just `git commit` and push the changes.
You'll be a `git pull` away from syncing them across other machines!

Some programs expect their configs in `~/`.
Handle them like [this](https://github.com/EricDriussi/dotfiles/tree/master/base).

Others expect them under `~/.config/`.
No problem: just as we explained, create a [mirror image](https://github.com/EricDriussi/dotfiles/tree/master/picom/.config) and let stow take care of it.

This, apart from being **incredibly** easy to keep track of these files, allows us to easily use `git` to version (and share!) them.
Moreover, it makes it a lot easier to [automate](https://gitlab.com/ericdriussi/sys-init/-/blob/master/tasks/config-dotfiles.yml#L38) the config process of your favorite OS.

### Migration

More than likely, if you try to use `stow` on a system already quite configured it won't be happy.

This is because, if it detects files where it wants to establish symlinks it will let you know and abort.

There are flags available to tell `stow` to stomp over whatever it finds, but I would advise you **not** to follow this approach.

Take your time when setting this up. Go one by one, moving the file or directory over to your dotfiles directory.
Ensure the structure is correct, remove the old config and run `stow DIR_NAME` one by one.

You'll be happy you did!
