---
title: Life beyond Google Search
published: 2021-04-25T11:42:43+01:00
image: ../../assets/content/magnify.webp
category: write-ups
tags:
    - google
    - searx
    - duckduckgo
    - brave
    - search engine
description: Search engines worth using
updated: Jan 29 2023
---

Even setting aside all the privacy concerns that come with using any Google product, some find the almighty search engine to be pretty lack luster.

Its results are filled with ads, spam and irrelevant or even auto-generated results.
Plus, it's nearly impossible to sift through the noise when investigating any sort of vaguely controversial topic.

To be fair, there is one thing that it does pretty well: help you out when you don't quite know what you are looking for.
I wouldn't say it does so to your best interest but hey, it's something.

## Search Engines

There are quite a few search engines available to you.
Some of them (Bing) are widely considered meme-engines.

I would disagree: each of them has its own use case.
We are just used to the (supposed) omnipotence of Google.
Here is a quick overview:

### [Duckduckgo](https://duckduckgo.com)

Probably the most popular of the bunch.
Privacy minded, kind of bare bones.
No tracking, no profiling and much fewer ads than Google.
Pretty good general purpose alternative, except maybe for image search.

### [Startpage](https://startpage.com)

A different front-end to Google's back end.
The idea is that you still want Google's results (for some reason) but would rather not have the NSA over for dinner.
Most of the targeted advertisement [should](https://github.com/prism-break/prism-break/issues/168) not spam your results.
They are based in Europe which might give you some peace of mind.

### [Swisscows](https://swisscows.com)

Super family friendly SE.
Built-in blockage of porn, violence and the likes.
It mixes its own indexing with Bing's.

### [Bing](https://bing.com)

Indeed, trusting Microsoft instead of Google is hardly any better.
However, it's preferable to have 5 different companies partially tracking you than to have one knowing you better than you know yourself.
They do their own indexing, so results should be more or less independent of Google.
Plus, it's actually pretty good for image searches.

### [Yahoo](https://yahoo.com)

The same reasoning as above more or less applies here as well. Still not a great service privacy wise, but useful in its own right.
If you are into crypto or finance in general, it has some pretty useful tools and is well respected in that regard.

### [Qwant](https://www.qwant.com)

Based in France, it recently started doing their own indexing.
Easy to use, simple UI, user-friendly design.

### [Wolframalpha](https://www.wolframalpha.com)

Mainly used in academia.
Rather different from what we usually understand by _'Search Engine'_, but rather useful with technical searches.

### [Yandex](https://yandex.com)

For those who hate the NSA but would love to meet the ~~KGB~~ SVR.
Jokes aside, it's very widely used along Russia's sphere of influence.
As such, it's very useful for learning different perspectives on sensitive issues or topics.

If you want to '**legally**' download content, look no further.

### [You.com](https://you.com/)

Pretty UI, basically no ads, and the ability to customize sources and searches to your heart's content.
What's not to like?

### [Brave search](https://search.brave.com/)

Another one of the few that do their own indexing.
Pretty fast and reliable. Their whole marketing revolves around privacy on the web, so you can expect a decent level of privacy.
It even offers to add results from other SE in case you aren't satisfied.

It also works on [Tor](https://search.brave4u7jddbv7cyviptqjc7jusxh72uik7zt6adtckl5f4nwy2v72qd.onion/)!!

## Search with Searx

_So what now? Am I supposed to use twelve search engines instead of one?_

![yesbutno](./yesbutno.webp)

You can just use a **meta SE**!
Simply put, it queries a bunch of different SE for you and presents all the results in a single page.

![meta-search](./meta-search.webp)

The main one that comes to mind is Searx (or more accurately, it's fork SearxNG).

- It doesn't offer personalized results, because it doesn't generate a profile about you.
- It doesn't save or share what you search for.
- It's fully open source (code [here](https://github.com/searxng/searxng)) so you can actually host your own instance (I actually do so and use it daily) or just chose one you trust from [this](https://searx.space) list and use it.

If you want you can set exactly how and which SE it queries. If you don't, you can just go ahead and use a public instance as is.
It won't work with every SE available, and it might be a bit fiddly on occasion, but it probably offers you more than you need, and it's just so convenient there's no getting around it.
