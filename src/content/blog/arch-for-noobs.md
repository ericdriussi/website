---
title: Software Architecture for Noobs
published: 2021-11-27T12:14:39Z
image: ../../assets/content/architecture.webp
series: architecture
category: best-practices
tags:
    - architecture
    - design
    - onion
    - mvc
    - ebi
    - clean
    - ports and adapters
description: A place to start learning about Software Architecture
updated: Nov 17 2024
---

Some posts overviewing some code architecture patterns I've seen being used in one way or another either directly or indirectly through the ideas they bring to the table.

Best to read them in order, since newer concepts are often better understood with the previous ones in mind.

These posts are not about low level architecture nor whiteboard-like systems design. Rather we go over different ways to structure code to make it (hopefully) more maintainable and easier to work with in the long run.

Apart from reading the source material for each architecture, you can go in much more detail in [this great series of posts](https://herbertograca.com/2017/07/03/the-software-architecture-chronicles/).

Seriously go check that out, it's great. In the meanwhile:

1. [MVC-MVVM](../mvc-arch)
1. [EBI](../ebi-arch)
1. [DDD (Tactical)](../ddd-tactics)
1. [DDD (Strategical)](../ddd-strategy)
1. [Ports & Adapters](../ports-and-adapters)
1. [Onion](../onion-arch)
1. [Clean](../clean-arch)

Have fun!
