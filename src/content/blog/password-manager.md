---
title: Please use a password manager
published: 2021-03-31T19:49:26+01:00
image: ../../assets/content/pwd.webp
category: tools
tags:
    - password manager
    - bitwarden
    - keepassxc
description: You need one whether you know it or not
updated: Jun 03 2023
---

## Why you need it

### Repeated passwords

Even if you think its new and clever, you might very well have used the password you just *'invented'* for a long forgotten account (which may or may not have leaked).
Repeating passwords is nearly as bad as setting them to '*admin*' or '*password1*'.

### Plain text

If you don't use a password manager and don't repeat passwords, chances are you are storing them in an unencrypted, plain text file.

We all manage a **huge** amount of accounts, no way you can remember all those passwords.

Anybody with (even remote) access to your machine can read an unencrypted file.
Plus you need to be in that specific machine to access your passwords or copy that file around.

### TOTP

Nowadays, it's often required to have some sort of MFA set up.
**O**ne **T**ime **P**asswords are by far the mos convenient and secure way to achieve this.

Plain and simple, this is not possible without a password manager.

### Work passwords

You might not care about your personal stuff, but please do care about your work related accounts and credentials.

You put your whole company, coworkers and clients/users at risk when you neglect your online security at work.

One of the main ways attackers get access to user's sensitive information is by taking advantage of bad practices used by the people who are supposed to be trusted with that information.

## Why you want it

### It's more comfy than your solution

I can bet that the way you currently manage your passwords is either uncomfortable or insecure.
You either have them written in plain text in a file you have to fetch every time you log in (or even worse, written in a physical paper like a **caveman**), or you let your browser manage them for you (good luck using a different browser or needing any kind of advanced management).

Good password managers, especially if they have a companion browser extension, are literally a one click solution to both creating good passwords and filling them into the login forms.

### Good passwords are hard

Just look at the requirements for any account password and be honest: Can you really come up with a good one without using personal information like name or DOB?
Yeah, me neither.

### Typing huge passwords sucks

And you always get something wrong.

### Lost the file? Lost all passwords

If store them in a file, your passwords are **gone forever** as soon as that file gets deleted.

![no](./no.webp)

That's just a bummer.

## What to use

Well... a password manager 😀. Here is what to avoid and a personal suggestion.

### Avoid non FOS software. Here is why

1. Nobody knows what the code actually does or how secure it is. You are 100% just **trusting** the company offering the service.
2. FOSS is always **more secure**. It can be publicly audited and people **will** pick it apart and patch it.
3. If the company decides to make you pay for features that where once free you might have no choice, except **maybe** to export a JSON or CSV file and move away.
4. If the company goes six feet under, you're on a ticking time bomb to find an alternative.
5. You are in charge. You don't have to, but often **can** go and host the service [yourself](https://vault.devintheshell.xyz).

### The dynamic duo

#### [BitWarden](https://bitwarden.com)

The more user friendly alternative.
They are widely used and known, are repeatedly audited by third parties, have a free and a paid business service, and have pretty much anything you might need:

- Desktop GUI
- Desktop CLI
- Mobile GUI
- Browser Plugin
- Web Vault

You have the option to make an account with them and host your passwords in their servers (just like with any other password manager) **or** you can host your own instance on your own server.

If you plan to go that route, check out [VaultWarden](https://github.com/dani-garcia/vaultwarden) for a super lightweight alternative!

#### [KeePassXC](https://keepassxc.org)

Minimal solution (although not as minimalist as just using [pass](https://www.passwordstore.org)).
It's a cross-platform implementation of the [KeePass](https://wiki.archlinux.org/index.php/KeePass) standard with added plugin support.

You have a local encrypted vault which you connect to the plugin and that's it.
**You** are in charge of backups and security and can access the vault only locally, but there is literally no one else involved.
Not even a connection the web.

## Conclusion

Personally I have used a lot of different password managers.
Nowadays, I run a [VaultWarden](https://github.com/dani-garcia/vaultwarden) instance on a VPS but still have a local copy available from KeePassXC, just in case.

No solution is perfect for everyone and each have valid use cases.

Except for not using one.
That's just silly 🙃.
