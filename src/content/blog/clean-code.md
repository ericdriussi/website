---
title: Clean Your Code
published: 2021-03-24T14:52:21Z
image: ../../assets/content/uncleBob.webp
category: best-practices
tags:
    - clean code
    - uncle bob
    - Robert C. Martin
description: High level overview of Uncle Bob's Clean Code points and core concepts
updated: Mar 06 2023
---

Got the idea from [here](https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29), but concepts below come mostly from horizontal reading and Uncle Bob's speeches.

## Overview

As a general rule of thumb, try to make your code _'pretty'_.

Look at the indentations, do they make sense? Are you like five indentation levels deep? Is the naming descriptive? Is there a more efficient way to achieve the same thing? Is your code elegant?
Do you actually feel proud about it and have an urge to show it to your peers?

### Made to be read

You got your code to work? Great!
That means it's machine friendly. Now you need to go back and make it human friendly.

When coming up with new ideas or solutions to existing problems, our minds tend to become a bit messy.
We forget stuff, rush through ideas and generally don't care much for the _'proper solution'_ but focus on _'a solution'_.

This is good, we **should** focus on getting the job done first.
But we also need to take some time after the fact to clean after ourselves.

You are most likely not the only one working on that peace of code, treat it like a common space.
First make it work, then clean it up.

> You're not done when it works, You're done when it's right.
> — <cite>Robert C. Martin<cite>

Code is clean if it can be understood easily by a competent reader.
Another developer should be able to enhance it (or fix it for that matter).

![wtfdoor](./wtfdoor.webp)

Surprises might be nice irl but I'm not looking forward to going _"WTF?"_ when reading your code.
The more predictable the better.

> Clean code does one thing well.
> — <cite>Bjarne Stroustrup <cite>

## Some Rules

### General

1. **Conventions** are there for a reason. It's easier for me to understand you if we both follow a set of common rules.
2. **KISS** (Keep it simple stupid). Reduce complexity as much as possible, keep it _minimalistic_.
3. Always find the **root cause**. Don't just fix the problem, actually fully resolve the issue.
4. **Boy Scout Rule**. Leave the code better than you found it.
5. **Unix** philosophy. Do one thing and do it well, don't write code with excessive/diverse responsibilities.

### Understandability

1. Be **consistent**. If class A has a `var name` and a `var surname`, class B shouldn't have a `var firsName` and a `var lastName` in their place.
2. Use **explanatory** variables names. They help with semantics.
3. Prefer **value objects** to primitive type. They also help with semantics.
4. Avoid **negative conditionals** and be generally careful with conditionals, they get out of hand fast.

### Naming

1. They should be descriptive, unambiguous, meaningful, pronounceable and searchable.
2. Don't use `data` or `object` in the name. We already know.
3. No Magic Numbers.

### Functions

1. They should be **small** and **single scoped**.
2. Their names should describe their intent. Default to using **verbs** if possible.
3. No more than 3 **arguments**, the fewer, the better.
4. **Side effects** are usually bad, hard to track and easy to avoid.
5. Don't use **flag** arguments. If a function does one thing or another based on the input, you should have two functions, not one.

### Comments

1. A **necessary evil**. To be avoided if possible.
2. Trust your VCS, **remove** the code.
3. Use them to: explain the **intent**, **clarify** the code or **warn** of consequences.

### Structure

1. Prefer **vertical** coupling/cohesion rather than horizontal.
2. **Group** variables, objects and functions if they relate in usage.
3. Respect **indentation**.
4. Keep **lines short**.
5. **Blank lines** can be used to separate weakly related elements. Consider separating them further.

### Objects

1. Expose an **interface (API)**, hide internal structure.
2. Keep them **small** and **single scoped**.
3. Few imports and few **instance variables**.

### Avoid

1. **Rigid** software is hard to change, and there are usually cascading consequences for each change.
2. **Fragile** code breaks in many places due to a single change.
3. **Complexity** is sometimes _necessary_, but often _accidental_. Avoid the latter.
4. Code **Repetition** is a pain to work with.
5. **Opacity** is not a sign of a clever mind, but of an uncaring personality.
