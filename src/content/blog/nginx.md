---
title: Serve your website with Nginx
published: 2021-07-12T11:50:30+01:00
image: ../../assets/content/nginx-logo.webp
category: self-host
tags:
    - website
    - nginx
    - vps
description: Quick and easy
updated: Jan 29 2023
---

You are going to need a server or a VPS, as well as a registered domain name.

## Basics

Assuming you already have a properly configured [shh connection](../maintain-vps#ssh), connect to your VPS and install NGINX.

Broadly speaking, NGINX will look for instructions on how to serve a given site in the `sites-enabled` directory.
These are usually symlinks to config files located under `sites-available`.

Let's learn by building something.

### Site level config

Say you have your website under `/path/to/your/website/`, just as an example.
We'll begin by creating a config for your website:

```sh
nano /etc/nginx/sites-available/yourwebsite
```

Of course, name `yourwebsite` whatever you like.

Just copy the config below, swapping `yourdomain.org` for whatever your domain is.

```nginx
server {
    listen 80 ;
    listen [::]:80 ;
    server_name yourdomain.org ;
    root /path/to/your/website ;
    index index.html ;
    location / {
        try_files $uri $uri/ =404 ;
    }
}
```

It's pretty self-explanatory:
*"Listen on port 80 (HTTP) for requests to `yourdomain.org`, and serve them whatever is under `/path/to/your/website`, starting with the `index.html` file. If nothing is there, respond with code 404."*

### Enable the site

Use `ln -s` to symlink the config file as explained above and restart nginx to make it load the new configurations:

```sh
ln -s /etc/nginx/sites-available/yourwebsite /etc/nginx/sites-enabled
systemctl restart nginx
```

We are basically done at this point!
This will serve your website under your domain name. It's really that easy.

That said, there are some other things you might want to consider.

## NGINX config

We went over how to configure NGINX at the site level.
There is also a more general config file: `/etc/nginx/nginx.conf`
Here you can tinker with more generic settings regarding NGINX itself.

For example, if you are hosting a file server you might want to play around with `client_max_body_size`.
This will change the maximum allowed size of the client request bodies.

You can change where the access or error logs are stored with `access_log` and `error_log`.

Or you could please the SEO gods with these Gzip settings:

```nginx
gzip on;
gzip_vary on;
gzip_min_length 10240;
gzip_proxied expired no-cache no-store private auth;
gzip_types text/plain text/css text/xml text/javascript application/x-javascript application/xml application/json;
gzip_disable "MSIE [1-6]\.";
```

## Security

As a general rule, you want to at least have a [properly set up firewall](../ufw).

Moreover, your browser will try to prevent you from visiting your site.
This is due to the lack of certificates, modern browsers really prefer HTTPS (and for good reasons).

Let's see if we can get that lock icon that browsers like so much.

### Be the S in HTTPS

There are multiple ways to obtain certificates for your site, but by far the easiest is to use `certbot`.

```sh
apt install python-certbot-nginx
certbot --nginx
```

Just follow the instructions, it's dead easy.
It'll ask for:

- Your email (to notify about expiring certs)
- Which domain/s to certify
- Whether to redirect traffic from HTTP to HTTPS. This is definitely the way to go, so feel free to select option 2 here.

Certificates created with this method need to be renewed every three months.
We can automate this using cron.

```sh
crontab -e
```

Paste `0 0 2 * * certbot --nginx renew` into the file to create a cronjob that automatically asks `certbot` to renew all certs, and does so every two months.

Now we have a decent NGINX setup as a starting point for your website or for whatever else you might want to host on your [VPS](../maintain-vps)
