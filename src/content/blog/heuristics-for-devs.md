---
title: Heuristics for Devs
pinned: true
published: 2022-01-29T16:17:57Z
image: ../../assets/content/hermit.webp
category: write-ups
tags:
    - architecture
    - design
    - code
description: Growing list of code, design and development related heuristics
updated: Nov 18 2024
---

Some heuristics I find useful at work. Something like [this](https://github.com/stanislaw/SoftwareDesignHeuristics), but dumbed down and more concise.

These are not meant to be dogmas but general rules of thumb that should help you be a better dev. Ditch them as soon as they don't.

## Fast Feedback

Both at small (TDD) and large (CD) scale.
Don't just _think_ it's OK, actually see if it is in practice.

Expect fuckups to occur, try to know about them fast.

## Baby steps

### Refactoring

Start small, even if small means apparently irrelevant, peripheral changes (variable names, directory structure, etc.).

### New Feature

Find the smallest meaningful piece of the system and build up from there.

## Complexity

The root of all evil. It is **sometimes necessary**, but often accidental.
Be skeptical of the former and avoid the latter at all cost.

## Divide and conquer

Split everything up as much as possible, even if it seems absurd.
As long as it doesn't take more effort to split the task than to actually do it, the smaller, the better.

## Decide as late as possible

Chances are, the later you decide, the more knowledge and experience you have.
Anything that **can** be decided further down the road without causing a major setback, **should**.

## Respect the Legacy

We know at least two things about all Legacy Code:

- It works.
- It makes money.

Respect Legacy Code and the people who wrote it.

## Go fast, write great code

The only sustainable way to go fast is writing great code.
The only sustainable way to write great code is going fast.

## Start anew

Don't build the project of tomorrow with the crap from yesterday.

## Listen to your gut feeling

Don't dismiss it with a _'meh, it works'_.
If you feel something could be better, **make it better**.

## Be water my friend

Flexible with other peoples code, strict with yours.

## Commits

Keep 'em coming and keep 'em small.
Think of them as checkpoints, safe states you can return to. You can always squash them later.

## Boy Scout Rule

Leave the system (code, docs, etc.) better than you found it.

## Code

### Readable

Code for other people, not for the CPU.

### Naming is not relevant

It's **extremely** important.

### Simplistic naming

Complex naming schemes might indicate inadequate modelling.

### Expl!c!t language

When in doubt be explicit.

### Boring, repetitive, predictable

Boring code is good code.
No surprises, no 'WTF'.

### Write code like a manual

Show what it does and how to use it.
Hide how it works.

### Syntax

Nouns for classes.
Verbs for functions.
Adjectives for interfaces.

## Tests

### Isolation

Not all layers need to be tested in isolation, or tested at all for that matter.

### Test Behavior

Not code.

### Coverage

Low test coverage suggests you might want to write more/better tests.
High test coverage **does not** imply you are testing enough/properly.

### Don't test someone else's code

Either trust the framework/library or chose another one.
If you can't trust it, don't depend on it.

## Design

### Unix

Do 'one thing', do it well.

### Abstract dependencies

Depend on abstractions, not concretions.

### Extend existing behavior

Don't modify it.

### Coupling and Cohesion

They are the same thing. The latter just has some though into it.

### Demeter, don't ask

Units (classes, modules, functions) should talk to one another only if they share the same concern, and in such a way that keeps them ignorant of one another's inner workings.

### Avoid changes in abstraction levels

They are hard to follow and indicate that something might need to be in a different layer.

### Avoid generalizations

They are easy to build but a pain to remove.

## Don'ts

### Don't do a perfect job

Perfection is hardly relevant. A bad test is better than no test.
Don't waste time and mental space on perfection.

### Don't follow the rules

Follow the principles.

### Don't be clever

Don't get fancy. Keep it simple.

### Don't be an 'architect'

Bug-less code with meh-architecture is better than awesome architecture with buggy code.

### Don't fear duplication

It's better than poor abstraction.
The [DRY](https://en.wikipedia.org/wiki/Don't_repeat_yourself) principle is about avoiding duplicate **logic or knowledge**.

Duplicate lines might imply logic duplication, but they might not.

### Don't get sentimental

No emotional attachment to code. Not to yours, not to others.
