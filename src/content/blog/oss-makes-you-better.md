---
title: Selfish reasons to engage in Open Source
description: How OSS can help you be a better dev
image: ../../assets/content/dark_cookies.png
category: write-ups
tags:
  - open source
  - oss
  - soft skills
published: 07 Feb 2025
---

There are many reasons to use, make or contribute to OSS.
While the ethical and societal benefits might be enough for some, I'd like to argue there are also purely selfish reasons for a developer to get involved.

Indeed, just like running Linux in your workstation deepens your knowledge of operating systems and broadens your tool-set basically for free, so too can OSS make you a better developer pretty much by osmosis.

Here are a bunch of ways this osmosis happens.

## Other people's code

Contrary to what I thought when starting out, the bulk of our work consists of **navigating, understanding and modifying** other people's code.
And yes, _"other people's code"_ includes the code you wrote a year ago.

This skill can hardly be trained or practiced as it should: you can type faster, practice problem-solving or read a bunch of books, but nothing trains you to deal with systems you didn't write.
In OSS however you'll be doing this all the time.

Very different people from different walks of life contribute to OSS, I've seen ways of writing and structuring code that I could have never imagined (both good and bad).
Participating in this space will allow you to improve this skill set.

You might think something like _"no, thanks, I get plenty of that at work"_, but is that really the case?
Mostly, I see people getting accustomed to their teammates code, creating a sort of cesspool of the lowest common denominator.
At work, we learn to navigate **a specific codebase**, written by specific people, instead of learning the skill of code navigation more broadly.
This is a shame.

## Documentation

Maybe I've just been very lucky (or unlucky, depending on how you look at it), but I have never, not even once, seen documentation half as good in proprietary projects as I have in OSS projects. Both dev-facing and user-facing.

This not only means that the latter ones are a joy to work with by comparison, but also that I have a chance to **learn** what makes documentation good and how to write it myself.

There is no asking around co-workers to understand how to build the project, what ritual to follow when modifying the system ([if any](../tbd-why/)) or the general structure of the code base.
Any reasonably complex OSS project that actually expects and welcomes contributions will at least have a `CONTRIBUTING.md` file to document all these issues and many more.
We should normalize this for proprietary projects as well.

The same goes for user-facing documentation: where companies have on-boarding teams and an endless supply of outdated docs that often force users to open support tickets, OSS projects have a compact, to-the-point `README.md` for basic instructions and very little incentive to offer free support (which means the docs are usually kept up to date and users are expected to actually read them).

Sure, on-boarding teams often serve a more service-oriented purpose. Something like _"tell me what to do, I don't want to waste my time reading your docs"_.
Still, concise, to the point documentation is often just as, if not more, valuable than having someone to directly ask for help.

## Troubleshooting

There are plenty of ways to help OSS projects, well written bug reports are one of them, and they don't get the respect they deserve.
[A good bug report](https://github.com/jdx/mise/discussions/3965) can literally be [used as a test case](https://github.com/jdx/mise/pull/4010/files#diff-2f4ca92b70774de1683c7265e99c20689bd7bccdf8bde6448276f8e1f85e4a34).
Writing a decent, well-structured bug report is non-trivial and can teach **a lot** about the troubleshooting process in general.

It is common for projects to have some sort of template or required information for the bug report. Something like versions of relevant software, configurations, expected behavior and minimum reproducible setup.

Not only can one learn a lot by paying attention to what information they require, but the act of gathering it and writing the report usually already starts the troubleshooting process for the maintainer.

I'm not sure how to describe it, but this process trains a sort of mind-set that can save **a lot of time** when things go wrong and help you get to the crux of the issue instead of fumbling around blindly.

## Soft skills

These are often average at best in developers and understandably so: they are only indirectly related to writing code and are quite difficult to train.
They are however really important and often make the difference between a good dev and a great one.
OSS can be used as a playground to improve them, including but not limited to:

### Communication

When participating in OSS projects, you will receive **criticism**. This is needed to keep the projects afloat and while it often comes with care and good intentions, this is not always the case.
This is a great chance to learn how to handle and respond to it, with the added benefit of not putting your job at risk.

Giving and receiving constructive **feedback** during code reviews hones your ability to communicate professionally and respectfully.

You will also have to communicate with **clarity**, since there is no _"let's quickly hop on a call"_ when talking to strangers on the internet. At least not usually.
Writing issues, participating in discussions, writing or improving documentation: these all force you to articulate complex ideas **clearly and succinctly**, including all necessary information and absolutely no unneeded padding.

### Interpersonal

More often than not, you'll have to **adapt** to the preferences of either the maintainer of a project or the wider community.
This might not seem like a great thing, but learning to _disagree and commit_ is rather important especially in a business setting, as the alternative often consists of endless, bitter discussions with teammates and co-workers.

It takes some **empathy** to understand that people you'll never meet might have the same general goals and good intentions you have, while disagreeing with this or that particular point.
It takes some more empathy to not get mad at a user telling you that the default behavior of your software makes no sense, or that a given feature is actually a bug.
Sure it's frustrating, but how often do we have the luxury of talking directly with the end-users of our software?

Another source of interpersonal friction is a lack of **cultural awareness**. It's easy to forget that you might be interpreting as rude something that is totally respectful for a different culture.
It's even harder to think of a better environment to get used to this than OSS, short of a language exchange bar.

Sure, communication is mainly in English (which already cuts out non-English speakers), but you'd be surprised how many of them are not using their first language.
Open any issue tracker or online discussion, you might be reading a German dev responding to a Chinese user.
This matters a lot and changes one's approach to a conversation entirely.
Rarely have I been in a work environment with comparable diversity.
