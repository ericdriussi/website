---
title: Pixar Driven Development
published: 2022-02-13T11:53:06Z
image: ../../assets/content/pixar-lamp.webp
category: write-ups
tags:
    - pixar
    - development
description: Not really a thing
updated: Jan 29 2023
---

Weather you personally like them or not, it's hard to argue the impact that Pixar films have on the vast majority of people and the rest of the industry.

I'm sure you know who Buzz Light Year is, and any 90s kid can recognize the sound of the Pixar lamp from a mile away.

![lamp](./pixar-jump.webp)

So [how do they achieve this](https://www.youtube.com/watch?v=rkvuOLuoTfs)? And why on earth should software developers care at all?

## Technical Similarities

There are a couple of techniques they follow that sound surprisingly similar to some of the best practices in software development.

See if any of the following sound familiar:

### Not over yet

When writing the script, they don't just get the script done first and then continue with the production.

They keep writing and improving the script until the whole film is **completely** finished (as in actually being released).

So the _development_ of the script is _continuous_, and the _integration_ with the rest of the production team is constant and changing.

### Refactor that scene

The opening of Toy Story 3 was re-written 60 times to get it just right.

No matter how good the first _iteration_ is, you can almost certainly make it better.

### Break the script down into Bounded Contexts

Once the script has the minimum basic structure and general shape, they will _break it down_ into sequences: relatively small story arcs that, although all connected, are somewhat independent of one another.

They look for about 25 to 30 sequences per film and assign them to different teams which can work on them (and give each other feedback) _concurrently_.

What an _Agile_ approach!

### Fast Feedback

**While** all of this is happening, a storyboard is created. Not before the work begins, not after it's done, but **while** it's taking place.

This allows for a broader, more birds eye view of the project and gives a clear picture of _what works and what doesn't_.

### Communication

Obviously, none of the above are possible without constant communication.

This is not a blanket statement: communication between teams, departments and individuals is absolutely **key** to making this work.

### Digital in Nature

There are clear differences between live action movies and animated ones, mainly due to the digital nature of the latter.
Carrying over the limitations of the _real world_ to the digital space makes no sense. Why not take advantage of the differences?

In that spirit, they create a rough draft of the entire film. Everything from fake voice acting done by employees to very rough animations.
Doing so allows them to modify animations, adjust the script and the sequences however the like.

After all, it's not like they need to record the whole movie with real actors and only then edit the scenes working with whatever they got.

It's really kind of strange the amount of influence the manufacturing industry has had over software development when you think about it.

## Let them write

Another thing Pixar does well is hiring the right talent for the job at hand.

They understand not all animators and/or writers are the same nor are they always good at everything vaguely related to their profession.

Of course, this is easier said than done.
In most cases companies have to work with what they can find and/or employ.

There is one thing to keep in mind here: Writers work with total creative freedom, no oversight, no deadlines.
You can imagine this sounds like heaven to a writer.

This creates a positive feedback loop where Pixar seeks out the best talent and facilitates a work environment in which pretty much everyone would want to work, which in turn makes finding that talent a lot easier.

## Collaboration

None of the movies are the result of one brave employee who knows best and has all the skills.

To set an example, the creation of _Toy Story 3_ involved all the top level creative people going **together** to a cabin in the woods.
After just 2 days they had the basic premise and ideas for the movie.

These were 6 creative minded people coming together and reaching an **agreement** (in very little time as well).
This trickles down to the rest of the production team.

Every moment of every scene is the fruit of the work and attention to details of hundreds of employees in constant collaboration.

Directors work with writers, writers work with animators, animators with actors, and so on.

It's a network of hard work, trust and harmony that makes the whole deal not only work, but work amazingly well and produces some of the most memorable films of my generation.

## The rules

Let me leave you with an extract of [Pixar 22 rules of storytelling](https://www.aerogrammestudio.com/2013/03/07/pixars-22-rules-of-storytelling/).
See if you can spot the similarities:

- You gotta keep in mind what’s interesting to you as an audience, not what’s fun to do as a writer. They can be very different.
- Trying for theme is important, but you won’t see what the story is actually about til you’re at the end of it.
- Simplify. Focus. Combine characters. Hop over detours. You’ll feel like you’re losing valuable stuff, but it sets you free.
- What is your character good at, comfortable with? Throw the polar opposite at them. Challenge them. How do they deal?
- Come up with your ending before you figure out your middle. Seriously. Endings are hard, get yours working up front.
- Finish your story, let go even if it’s not perfect. In an ideal world you have both, but move on. Do better next time.
- Putting it on paper lets you start fixing it. If it stays in your head, a perfect idea, you’ll never share it with anyone.
- No work is ever wasted. If it’s not working, let go and move on – it’ll come back around to be useful later.
