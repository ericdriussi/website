---
published: 2021-07-12T17:14:24+01:00
title: Serve your site on Tor
image: ../../assets/content/onion.webp
category: self-host
tags:
    - website
    - tor
    - onion
description: Just because you can
updated: Jan 29 2023
---

## Why?

Isn't Tor used by criminals to do bad stuff?
[Kinda](https://2019.www.torproject.org/about/torusers.html.en). It's also used by people that cannot safely browse the clear web and/or express their opinion due to tough restrictions or straight systematic oppression.
People that don't want certain queries to be public, journalists, whistleblowers and privacy minded people regularly browse the web through Tor.

It's really easy, so why not make your site accessible for Tor users? Consider that even if you are not using Tor today, you might in the future.

## Install & Enable

For any serious use, you should have a look at [their install instructions](https://community.torproject.org/onion-services/setup/install/).
To keep it simple, you can probably find reasonable up-to-date packages in your preferred distro's repositories.

Once installed, open `/etc/tor/torrc` in your favorite editor, search for the lines
`HiddenServiceDir /var/lib/tor/hidden_service/`
and
`HiddenServicePort 80 127.0.0.1:80`
and uncomment them.

Start the service with `systemctl enable --now tor`.

Get your onion address with `cat /var/lib/tor/hidden_service/hostname`.

## Serve

If you know how to [set up `nginx`](../nginx) this won't be anything new to you.

Simply create your `nginx` config file for the onion site by opening `/etc/nginx/sites-available/your-onion-website` with your favorite text editor.
Then, paste and adjust these lines:

```nginx
server {
   listen 80 ;
   root /var/www/onion-site ;
   index index.html ;
   server_name onion-address.onion ;
}
```

Ensure you point to the right path and onion address, and that it!

Make sure to symlink the config file to `/etc/nginx/sites-enabled` and reload `nginx`.

You should now be able to open your Tor browser, paste your onion address, and visit your site!
