---
title: Pair Programming 101
published: 2023-02-04T10:10:33Z
image: ../../assets/content/pair.webp
category: best-practices
tags:
    - pairing
    - pair programming
    - development
description: Values, Roles, How's and Dont's
updated: Mar 6 2023
---

A quick overview of things to keep in mind and best practices when pair programming.

## Values

Not too far away from the ones described in [XP](https://www.amazon.com/Extreme-Programming-Explained-Embrace-Change/dp/0321278658), the key [values](https://www.algolia.com/blog/engineering/pair-programming-roles-challenges-guiding-principles-and-tools/) to foster when working in pairs are as follows:

### Humility

Appreciate **feedback**. Learn to give and receive it.
Don’t be afraid of being wrong, **ask questions** when needed (especially if you feel blocked).

### Trust

Believe in yourself and (**especially**) in your partner.
Understand that everyone solves problems differently.
This is a good thing, **trust** that someone else's path will lead to a desirable outcome.

### Grit

Muster the courage to stick together, **challenge** and **motivate** each other.

### Care

Foster **teamwork**, care for your partner.
Be there for one another.

## Roles

The two main roles in pair programming are the **Driver** (the one at the keyboard) and the **Navigator** (the one helping out).
Contrary to popular belief, these roles entail much more than "*I type you look*".

### Driver

As a driver, you should do one thing and to it well: **Focus**.
Worry only about the smallest step possible, *"forget"* about the feature/task as a whole.

**Think small**: naming, algorithms, implementation details, etc. should be your main practical concerns.

Two key and often overlooked aspects of being the driver:

- **Think out loud**
It's hard for the pair to be on the same page if the navigator doesn't know what the driver is thinking.
Narrate your thought process.

- Be **open** to criticism/improvement/discussion
Part of the point of working in pairs is to question one another and reach the best approach possible.
Don't take suggestions as an insult, your navigator is just trying to help!

### Navigator

A good navigator should practice **tactical thinking**, reaching compromises when required.
On top of that, think about the **bigger task** at hand, constraints, API, performance, etc.

The navigator should be asking **good questions** to ensure the development follows a desirable path.
In this, **rabbit holes** should be avoided.

This role is in an advantageous position to **catch** typos, errors, etc. and should **beware of complexity** (especially if avoidable).

**Forcing breaks** when needed, keeping an eye out for **meetings** and urgent Slack messages are tasks generally best suited to this role.

If you are in this position, be sure to **help out** as much as possible and **take notes** for future reference.
You should have the relevant **resources** and **documentation** at hand, and should remember (or have a list of) the things that came up during the session that might need further investigation or analysis.

## Practice

On a more [technical](https://martinfowler.com/articles/on-pair-programming.html) note, here are some things to keep in mind as far as *"how to's"*.

### A pair of what?

Are Juniors supposed to pair? What about Seniors?
There are three possible combinations here, each with its strengths and weaknesses:

#### Novice–novice

Significantly better results than two novices working independently.
Still, this makes it hard for novices to develop **good habits**, since they lack a proper role model.

#### Expert–novice

Possibly the best practice for **mentoring** and for getting fresh air into the system, as the novice is likely to question established practices.
This is great, but the expert needs to have the patience to handle the situation and the novice might feel too intimidated to have a productive session.

#### Expert–expert

Best for productivity and, given that both understand pair programming, very likely to produce great results.
Still, two experts are unlikely to question established practices, so novel ways to solve problems will likely not come up in these sessions.

### Alternative styles

#### Ping-Pong

Best served with a side dish of TDD.
Simply put, switch roles on every TDD step:

1. I write a test (You are the navigator)
1. You make it pass (You are the driver)
1. I refactor (You are the navigator)
1. You write a test (You are the driver)
1. ...

#### Strong style

The one with the idea (and/or knowledge) sticks to being the navigator.

> I've got an idea, please take the keyboard!

Very useful to ensure knowledge transfer.

> For an idea to go from your head into the computer it MUST go through someone else's hands.

Here, the senior should be navigator and the junior should be driver.
This can also be applied with two experts while handing over a task, as it ensures the objective is achieved.

#### Mob-ish

The line between the two roles gets blurred out.

With some tasks the role separation might not fit very well, but it might still be advantageous to tackle the issue as a pair.
Simulating a mob programming session, the pair as a whole takes care of the responsibilities of both roles.

### Time management

It is extremely important to consider breaks as a natural part of your work as a dev, much more so when pairing.

This practice can be quite tiresome and is not sustainable if frequent breaks are not taken.
And proper breaks at that: no slack, no emails, no checking tech-support.
Get up, go for a glass of water, step outside, etc.

There are multiple ways of ingraining breaks into your pairing routine:

#### Methodical approach

The classic *"Pomodoro"* style:

1. Work for 25 min
1. Take a 5-min break
1. Work for 25 min
1. Take a 5-min break
1. Work for 25 min
1. Take a 20-min break

The time windows should be adjusted as needed, but some consistency should be kept (you either take 5-min breaks or 10-min breaks, not 5 now, 10 later and 8 afterwards...).

#### Organic approach

If (and only if) the team is well-adjusted to pairing and there is a safe work environment, going by *"gut feeling"* might work quite nicely.
Take breaks as needed, have a *"feel"* for when a good moment to take a break arrives.

#### Rotation dependent

Another approach is to sync your breaks with your role rotations.
This can work really well in teams that are used to pairing and tend to swap roles very frequently.

Of course, not all tasks will allow this and not all teams are okay with it.
But especially in a remote environment, it is often quite nice to do so when possible.

### Rotations

#### Role Rotation

Really helpful when people are new to pair programming, as they can quickly get a feel for how each role works.
This also keeps you on your feet, since it requires you to fully focus on you current role's responsibilities.

As to when to rotate, it can sync with your time management schema or go by user story.
Make a conscious decision about when and how to do this, avoid switching roles for no reason.

#### Pair Rotation

Useful to spread knowledge between people, and facilitate collective code ownership.
It requires that everyone on the team is willing (and capable) to work with each other.

It keeps things fresh, as different ideas and new point of views are likely to come up on a regular basis.

Usually it doesn't make much sense to rotate pairs multiple times a day.
Think of doing it each Sprint, every X days, or maybe with every new user story.

Different teams and workflows will allow for more or less frequency in this regard.

## Don'ts

Each team does pair programming in its very own unique way.
This is a good thing, practices like this should be adapted to the team's needs and/or abilities.

There are however a few things that are better avoided.

Don't...

- Drift apart, zone out, loose focus, look at your phone...
- Micromanage what your driver should do. This is **sometimes** welcome, especially if the driver is new and/or blocked.
- Be impatient. Leave some room for your driver to figure out the error.
- Stress out your partner.
- Marry the keyboard. Sharing is caring.
- Pair all the time. Your job is much more than coding, pairing does not apply when writing emails or researching topics.
